﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.IsolatedStorage;

namespace TopoSmeta
{
    internal class ConfigManager
    {
        private static ConfigManager instance;

        private const string configFileName = "TopoSmeta.conf";
        private Dictionary<string, string> settings = new Dictionary<string, string>();

        private ConfigManager()
        {
            ReadConfig();
        }

        public static ConfigManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ConfigManager();
                }
                return instance;
            }
        }

        public string this[string key]
        {
            get
            {
                if (settings.ContainsKey(key))
                    return settings[key];
                return null;
            }
            set
            {
                settings[key] = value;
            }
        }

        public void ReadConfig()
        {
            using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                try
                {
                    settings.Clear();
                    using (StreamReader reader = new StreamReader(
                        new IsolatedStorageFileStream(configFileName, FileMode.Open, store)))
                    {
                        while (!reader.EndOfStream)
                        {
                            try
                            {
                                string k = reader.ReadLine();
                                string v = reader.ReadLine();
                                settings[k] = v;
                            }
                            catch
                            {
                                
                            }
                        }
                        reader.Close();
                    }
                }
                catch
                {
                    // Do nothing
                }
            }
        }

        public void WriteConfig()
        {
            using (IsolatedStorageFile store = IsolatedStorageFile.GetUserStoreForApplication())
            {
                try
                {
                    using (StreamWriter writer = new StreamWriter(
                        new IsolatedStorageFileStream(configFileName, FileMode.Create, store)))
                    {
                        foreach(string k in settings.Keys)
                        {
                            string v = settings[k];
                            try
                            {
                                writer.WriteLine(k);
                                writer.WriteLine(v);
                            }
                            catch
                            {
                                // Do nothing
                            }
                        }
                        writer.Close();
                    }
                }
                catch(Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Ошибка");
                }
            }
        }
    }
}
