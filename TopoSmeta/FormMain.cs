﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO.IsolatedStorage;
using System.Text;
using System.Windows.Forms;

namespace TopoSmeta
{
    // Структура для хранения расценок на создание топографичестих планов (т. 9)
    public struct PlanPrice
    {
        public int Paragraph; // параграф таблицы 9
        public int Scale; // масштаб съемки
        public int Category; // категория сложности (1, 2, 3)
        public float Height; // высота сечения рельефа
        public int Territory; // вид территории (0 = незастроенная, 1 = застроенная, 2 = промпредприятие)
        public float Field; // цена на полевые работы
        public float Cameral; // цена на камеральные работы
        public string Description; // текстовое описание вида съемки (для отображения в списке выбора)

        // конструктор, полностью инициализирующий структуру
        public PlanPrice(int p, int s, int g, double h, int t, double f, double c, string d)
        {
            Paragraph = p;
            Scale = s;
            Category = g;
            Height = (float)h;
            Territory = t;
            Field = (float)f;
            Cameral = (float)c;
            Description = d;
        }
    }

    // Структура для хранения строки сметы
    public struct EstimateLine
    {
        public string N; // Номер по порядку
        public string Reason; // Обоснование
        public string Work; // Наименование работ
        public string Unit; // Единица измерения
        public string Count; // Количество
        public string Price; // Цена единицы, руб.
        public string Cost; // Стоимость, руб.

        // Конструктор, инициализирующий все поля заданными значениями
        public EstimateLine(string n, string reason, string work,
            string unit, string count, string price, string cost)
        {
            N = n;
            Reason = reason;
            Work = work;
            Unit = unit;
            Count = count;
            Price = price;
            Cost = cost;
        }
        
        // Конструктор, инициализирующий первое поле пустой строкой
        public EstimateLine(string reason, string work,
            string unit, string count, string price, string cost)
        {
            N = "";
            Reason = reason;
            Work = work;
            Unit = unit;
            Count = count;
            Price = price;
            Cost = cost;
        }

        // Конструктор, инициализирующий пустыми строками все поля, кроме первого
        public EstimateLine(string n)
        {
            N = n;
            Reason = "";
            Work = "";
            Unit = "";
            Count = "";
            Price = "";
            Cost = "";
        }

        // Конструктор, инициализирующий пустыми строками все поля, кроме первого и последнего
        public EstimateLine(string n, string cost)
        {
            N = n;
            Reason = "";
            Work = "";
            Unit = "";
            Count = "";
            Price = "";
            Cost = cost;
        }

        public bool isHeader
        {
            get
            {
                if (N != "" && Reason == "" && Work == "" && Unit == "" &&
                    Count == "" && Price == "" && Cost == "")
                    return true;
                else
                    return false;
            }
        }

        public bool isTotal
        {
            get
            {
                if (N != "" && Reason == "" && Work == "" && Unit == "" &&
                    Count == "" && Price == "" && Cost != "")
                    return true;
                else
                    return false;
            }
        }
    }

    public partial class FormMain : Form
    {
        // Цены на создание топографических планов (т. 9)
        private static PlanPrice[] planPrices = {
new PlanPrice( 1,500,1,0.25,0,1989,493,"незастроенная I категории сложности, масштаб 1:500, высота сечения рельефа 0.25 м" ),
new PlanPrice( 1,500,1,0.25,1,2578,870,"застроенная I категории сложности, масштаб 1:500, высота сечения рельефа 0.25 м" ),
new PlanPrice( 1,500,1,0.25,2,3352,1436,"промпредприятие I категории сложности, масштаб 1:500, высота сечения рельефа 0.25 м" ),
new PlanPrice( 2,500,2,0.25,0,2578,700,"незастроенная II категории сложности, масштаб 1:500, высота сечения рельефа 0.25 м" ),
new PlanPrice( 2,500,2,0.25,1,3481,1269,"застроенная II категории сложности, масштаб 1:500, высота сечения рельефа 0.25 м" ),
new PlanPrice( 2,500,2,0.25,2,4524,2093,"промпредприятие II категории сложности, масштаб 1:500, высота сечения рельефа 0.25 м" ),
new PlanPrice( 3,500,3,0.25,0,3402,859,"незастроенная III категории сложности, масштаб 1:500, высота сечения рельефа 0.25 м" ),
new PlanPrice( 3,500,3,0.25,1,4991,1692,"застроенная III категории сложности, масштаб 1:500, высота сечения рельефа 0.25 м" ),
new PlanPrice( 3,500,3,0.25,2,6488,2793,"промпредприятие III категории сложности, масштаб 1:500, высота сечения рельефа 0.25 м" ),
new PlanPrice( 4,500,1,0.5,0,1723,418,"незастроенная I категории сложности, масштаб 1:500, высота сечения рельефа 0.50 м" ),
new PlanPrice( 4,500,1,0.5,1,2233,737,"застроенная I категории сложности, масштаб 1:500, высота сечения рельефа 0.50 м" ),
new PlanPrice( 4,500,1,0.5,2,3007,1268,"промпредприятие I категории сложности, масштаб 1:500, высота сечения рельефа 0.50 м" ),
new PlanPrice( 5,500,2,0.5,0,2432,589,"незастроенная II категории сложности, масштаб 1:500, высота сечения рельефа 0.50 м" ),
new PlanPrice( 5,500,2,0.5,1,3284,1067,"застроенная II категории сложности, масштаб 1:500, высота сечения рельефа 0.50 м" ),
new PlanPrice( 5,500,2,0.5,2,4632,1938,"промпредприятие II категории сложности, масштаб 1:500, высота сечения рельефа 0.50 м" ),
new PlanPrice( 6,500,3,0.5,0,3288,791,"незастроенная III категории сложности, масштаб 1:500, высота сечения рельефа 0.50 м" ),
new PlanPrice( 6,500,3,0.5,1,4824,1559,"застроенная III категории сложности, масштаб 1:500, высота сечения рельефа 0.50 м" ),
new PlanPrice( 6,500,3,0.5,2,6195,2558,"промпредприятие III категории сложности, масштаб 1:500, высота сечения рельефа 0.50 м" ),
new PlanPrice( 7,500,1,1,0,1460,398,"незастроенная I категории сложности, масштаб 1:500, высота сечения рельефа 1.00 м" ),
new PlanPrice( 7,500,1,1,1,2043,620,"застроенная I категории сложности, масштаб 1:500, высота сечения рельефа 1.00 м" ),
new PlanPrice( 8,500,2,1,0,1965,552,"незастроенная II категории сложности, масштаб 1:500, высота сечения рельефа 1.00 м" ),
new PlanPrice( 8,500,2,1,1,3095,939,"застроенная II категории сложности, масштаб 1:500, высота сечения рельефа 1.00 м" ),
new PlanPrice( 9,500,3,1,0,2253,711,"незастроенная III категории сложности, масштаб 1:500, высота сечения рельефа 1.00 м" ),
new PlanPrice( 9,500,3,1,1,4591,1408,"застроенная III категории сложности, масштаб 1:500, высота сечения рельефа 1.00 м" ),
new PlanPrice( 10,1000,1,0.5,0,936,234,"незастроенная I категории сложности, масштаб 1:1000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 10,1000,1,0.5,1,1676,543,"застроенная I категории сложности, масштаб 1:1000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 10,1000,1,0.5,2,2429,1011,"промпредприятие I категории сложности, масштаб 1:1000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 11,1000,2,0.5,0,1430,343,"незастроенная II категории сложности, масштаб 1:1000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 11,1000,2,0.5,1,2258,734,"застроенная II категории сложности, масштаб 1:1000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 11,1000,2,0.5,2,3560,1481,"промпредприятие II категории сложности, масштаб 1:1000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 12,1000,3,0.5,0,1984,474,"незастроенная III категории сложности, масштаб 1:1000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 12,1000,3,0.5,1,3665,1167,"застроенная III категории сложности, масштаб 1:1000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 12,1000,3,0.5,2,5027,2053,"промпредприятие III категории сложности, масштаб 1:1000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 13,1000,1,1,0,759,227,"незастроенная I категории сложности, масштаб 1:1000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 13,1000,1,1,1,1555,439,"застроенная I категории сложности, масштаб 1:1000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 14,1000,2,1,0,1117,318,"незастроенная II категории сложности, масштаб 1:1000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 14,1000,2,1,1,2153,675,"застроенная II категории сложности, масштаб 1:1000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 15,1000,3,1,0,1380,422,"незастроенная III категории сложности, масштаб 1:1000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 15,1000,3,1,1,3464,1016,"застроенная III категории сложности, масштаб 1:1000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 16,2000,1,0.5,0,408,91,"незастроенная I категории сложности, масштаб 1:2000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 16,2000,1,0.5,1,1366,460,"застроенная I категории сложности, масштаб 1:2000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 16,2000,1,0.5,2,1777,759,"промпредприятие I категории сложности, масштаб 1:2000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 17,2000,2,0.5,0,804,174,"незастроенная II категории сложности, масштаб 1:2000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 17,2000,2,0.5,1,2152,700,"застроенная II категории сложности, масштаб 1:2000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 17,2000,2,0.5,2,2796,1288,"промпредприятие II категории сложности, масштаб 1:2000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 18,2000,3,0.5,0,1796,387,"незастроенная III категории сложности, масштаб 1:2000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 18,2000,3,0.5,1,3530,1163,"застроенная III категории сложности, масштаб 1:2000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 18,2000,3,0.5,2,4588,1919,"промпредприятие III категории сложности, масштаб 1:2000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 19,2000,1,1,0,344,86,"незастроенная I категории сложности, масштаб 1:2000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 19,2000,1,1,1,1316,418,"застроенная I категории сложности, масштаб 1:2000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 20,2000,2,1,0,674,159,"незастроенная II категории сложности, масштаб 1:2000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 20,2000,2,1,1,2081,662,"застроенная II категории сложности, масштаб 1:2000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 21,2000,3,1,0,1412,343,"незастроенная III категории сложности, масштаб 1:2000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 21,2000,3,1,1,3428,1101,"застроенная III категории сложности, масштаб 1:2000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 22,2000,1,2,0,292,80,"незастроенная I категории сложности, масштаб 1:2000, высота сечения рельефа 2.00 м" ),
new PlanPrice( 23,2000,2,2,0,545,145,"незастроенная II категории сложности, масштаб 1:2000, высота сечения рельефа 2.00 м" ),
new PlanPrice( 24,2000,3,2,0,1132,297,"незастроенная III категории сложности, масштаб 1:2000, высота сечения рельефа 2.00 м" ),
new PlanPrice( 25,5000,1,0.5,0,228,50,"незастроенная I категории сложности, масштаб 1:5000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 25,5000,1,0.5,1,798,248,"застроенная I категории сложности, масштаб 1:5000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 25,5000,1,0.5,2,1037,425,"промпредприятие I категории сложности, масштаб 1:5000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 26,5000,2,0.5,0,454,90,"незастроенная II категории сложности, масштаб 1:5000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 26,5000,2,0.5,1,1306,302,"застроенная II категории сложности, масштаб 1:5000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 26,5000,2,0.5,2,1698,696,"промпредприятие II категории сложности, масштаб 1:5000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 27,5000,3,0.5,0,982,196,"незастроенная III категории сложности, масштаб 1:5000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 27,5000,3,0.5,1,2160,608,"застроенная III категории сложности, масштаб 1:5000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 27,5000,3,0.5,2,2808,1179,"промпредприятие III категории сложности, масштаб 1:5000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 28,5000,1,1,0,191,49,"незастроенная I категории сложности, масштаб 1:5000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 28,5000,1,1,1,668,243,"застроенная I категории сложности, масштаб 1:5000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 28,5000,1,1,2,868,356,"промпредприятие I категории сложности, масштаб 1:5000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 29,5000,2,1,0,369,86,"незастроенная II категории сложности, масштаб 1:5000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 29,5000,2,1,1,1061,288,"застроенная II категории сложности, масштаб 1:5000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 29,5000,2,1,2,1379,566,"промпредприятие II категории сложности, масштаб 1:5000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 30,5000,3,1,0,801,196,"незастроенная III категории сложности, масштаб 1:5000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 30,5000,3,1,1,1762,608,"застроенная III категории сложности, масштаб 1:5000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 30,5000,3,1,2,2290,939,"промпредприятие III категории сложности, масштаб 1:5000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 31,5000,1,2,0,163,47,"незастроенная I категории сложности, масштаб 1:5000, высота сечения рельефа 2.00 м" ),
new PlanPrice( 32,5000,2,2,0,304,86,"незастроенная II категории сложности, масштаб 1:5000, высота сечения рельефа 2.00 м" ),
new PlanPrice( 33,5000,3,2,0,599,173,"незастроенная III категории сложности, масштаб 1:5000, высота сечения рельефа 2.00 м" ),
new PlanPrice( 34,5000,1,5,0,144,47,"незастроенная I категории сложности, масштаб 1:5000, высота сечения рельефа 5.00 м" ),
new PlanPrice( 35,5000,2,5,0,270,78,"незастроенная II категории сложности, масштаб 1:5000, высота сечения рельефа 5.00 м" ),
new PlanPrice( 36,5000,3,5,0,565,173,"незастроенная III категории сложности, масштаб 1:5000, высота сечения рельефа 5.00 м" ),
new PlanPrice( 37,10000,1,0.5,0,151,26,"незастроенная I категории сложности, масштаб 1:10000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 38,10000,2,0.5,0,297,44,"незастроенная II категории сложности, масштаб 1:10000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 39,10000,3,0.5,0,689,100,"незастроенная III категории сложности, масштаб 1:10000, высота сечения рельефа 0.50 м" ),
new PlanPrice( 40,10000,1,1,0,121,26,"незастроенная I категории сложности, масштаб 1:10000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 40,10000,1,1,1,424,129,"застроенная I категории сложности, масштаб 1:10000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 40,10000,1,1,2,551,226,"промпредприятие I категории сложности, масштаб 1:10000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 41,10000,2,1,0,234,44,"незастроенная II категории сложности, масштаб 1:10000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 41,10000,2,1,1,673,218,"застроенная II категории сложности, масштаб 1:10000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 41,10000,2,1,2,875,359,"промпредприятие II категории сложности, масштаб 1:10000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 42,10000,3,1,0,520,100,"незастроенная III категории сложности, масштаб 1:10000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 42,10000,3,1,1,1144,310,"застроенная III категории сложности, масштаб 1:10000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 42,10000,3,1,2,1487,609,"промпредприятие III категории сложности, масштаб 1:10000, высота сечения рельефа 1.00 м" ),
new PlanPrice( 43,10000,1,2,0,97,24,"незастроенная I категории сложности, масштаб 1:10000, высота сечения рельефа 2.00 м" ),
new PlanPrice( 44,10000,2,2,0,195,41,"незастроенная II категории сложности, масштаб 1:10000, высота сечения рельефа 2.00 м" ),
new PlanPrice( 45,10000,3,2,0,443,92,"незастроенная III категории сложности, масштаб 1:10000, высота сечения рельефа 2.00 м" ),
new PlanPrice( 46,10000,1,5,0,88,22,"незастроенная I категории сложности, масштаб 1:10000, высота сечения рельефа 5.00 м" ),
new PlanPrice( 47,10000,2,5,0,175,37,"незастроенная II категории сложности, масштаб 1:10000, высота сечения рельефа 5.00 м" ),
new PlanPrice( 48,10000,3,5,0,400,83,"незастроенная III категории сложности, масштаб 1:10000, высота сечения рельефа 5.00 м" )
               
                                };

        // Таблица 4 (расходы по внутреннему транспорту, % от сметной стоимости полевых работ)
        private static double[,] table4 = {
            {  8.75,  7.5,  6.25,  5.0,  3.75 },
            { 11.25, 10.0,  8.75,  7.5,  6.25 },
            { 13.75, 12.5, 11.25, 10.0,  8.75 },
            { 16.25, 15.0, 13.75, 12.5, 11.25 },
            { 18.75, 17.5, 16.25, 15.0, 13.75 },
            { 21.25, 20.0, 18.75, 17.5, 16.25 },
            { 23.75, 22.5, 21.25, 20.0, 18.75 },
            { 26.25, 25.0, 23.75, 22.5, 21.25 },
        };

        private ConfigManager configManager = ConfigManager.Instance;

        // Список строк сметной таблицы
        private List<EstimateLine> estimate = new List<EstimateLine>();

        // Объект, предоставляющий список регионов
        private Regions regions = Regions.Instance;

        // Итог вычисления сметной стоимости в текущих ценах (с НДС, если необходимо)
        private double finalTotal;

        // НДС (входит в итог)
        private double vat;

        private int nextRow; // Номер следующей строки таблицы, выводимой на новой странице

        public FormMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Инициализация списка выбора вида съемки в соответствии с таблицей 9
            for (int i = 0; i < planPrices.Length; i++)
            {
                comboBoxTerritory.Items.Add(planPrices[i].Description);
            }
            comboBoxTerritory.SelectedIndex = 0;
            // Инициализация начального значения списка выбора внешнего расстояния до базы
            comboBoxExtDist.SelectedIndex = 0;
            // Инициализация начального значения списка выбора внутреннего расстояния от базы
            comboBoxIntDist.SelectedIndex = 4;
            // Задание параметров диалога просмотра сметы перед печатью
            printPreviewDialog1.PrintPreviewControl.Zoom = 1;
            printPreviewDialog1.Width = 800;
            printPreviewDialog1.Height = 600;

            // Заполнение списка регионов
            foreach (string regionName in regions.RegionNames)
                comboBoxRegion.Items.Add(regionName);

            // Заполнение списка районных коэффициентов к заработной плате работников
            comboBoxSalaryFactor.Items.Add(1.1);
            comboBoxSalaryFactor.Items.Add(1.15);
            comboBoxSalaryFactor.Items.Add(1.2);
            comboBoxSalaryFactor.Items.Add(1.25);
            comboBoxSalaryFactor.Items.Add(1.3);
            comboBoxSalaryFactor.Items.Add(1.4);
            comboBoxSalaryFactor.Items.Add(1.5);
            comboBoxSalaryFactor.Items.Add(1.6);
            comboBoxSalaryFactor.Items.Add(1.7);
            comboBoxSalaryFactor.Items.Add(1.8);
            comboBoxSalaryFactor.Items.Add(1.9);
            comboBoxSalaryFactor.Items.Add(2.0);
            comboBoxSalaryFactor.SelectedIndex = 0;

            // Восстановление сохранённых настроек
            try
            {
                if (configManager["Contractor"] != null)
                {
                    textBoxContractor.Text = configManager["Contractor"];
                }
                if (configManager["Inflation"] != null)
                {
                    numericUpDownInflation.Value = decimal.Parse(configManager["Inflation"],
                        System.Globalization.NumberFormatInfo.InvariantInfo);
                }
                if (configManager["InflationReason"] != null)
                {
                    textBoxInflationReason.Text = configManager["InflationReason"];
                }
                if (configManager["Region"] != null)
                {
                    int i = int.Parse(configManager["Region"],
                        System.Globalization.NumberFormatInfo.InvariantInfo);
                    if (i < comboBoxRegion.Items.Count)
                        comboBoxRegion.SelectedIndex = i;
                }
                if (configManager["UseVAT"] != null)
                {
                    checkBoxVAT.Checked = bool.Parse(configManager["UseVAT"]);
                }
                if (configManager["SalaryFactor"] != null)
                {
                    int i = int.Parse(configManager["SalaryFactor"],
                        System.Globalization.NumberFormatInfo.InvariantInfo);
                    if (i < comboBoxSalaryFactor.Items.Count)
                        comboBoxSalaryFactor.SelectedIndex = i;
                }
            }
            catch
            {
                MessageBox.Show("Ошибка парсинга настроек", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Обрабатывается с целью не допустить одновременную установку
        // флажков "Составление плана подземных коммуникаций" и "только вертикальная съемка"
        private void checkBoxVertical_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxVertical.Checked)
                checkBoxUnderground.Checked = false;
        }

        // Обрабатывается с целью не допустить одновременную установку
        // флажков "Составление плана подземных коммуникаций" и "только вертикальная съемка"
        private void checkBoxUnderground_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxUnderground.Checked)
                checkBoxVertical.Checked = false;
        }

        // Создание сметы
        private void buttonEstimate_Click(object sender, EventArgs e)
        {
            // Создание инженерно-топографических планов местности
            
            // Площадь участка съемки
            double area = (double)numericUpDownArea.Value;
            // Вид съемки и расценки из табл. 9
            PlanPrice price = planPrices[comboBoxTerritory.SelectedIndex];
            // Коэффициент  к полевым работам для участков небольшой площади
            double kSmall = 1;
            switch (price.Scale)
            {
                case 500:
                    if (area <= 1f)
                        kSmall = 1.4;
                    else if (area <= 5f)
                        kSmall = 1.2;
                    break;
                case 1000:
                    if (area <= 5f)
                        kSmall = 1.4;
                    else if (area <= 25f)
                        kSmall = 1.2;
                    break;
                case 2000:
                    if (area <= 10f)
                        kSmall = 1.4;
                    else if (area <= 50f)
                        kSmall = 1.2;
                    break;
                case 5000:
                    if (area <= 20f)
                        kSmall = 1.4;
                    else if (area <= 100f)
                        kSmall = 1.2;
                    break;
                case 10000:
                    if (area <= 100f)
                        kSmall = 1.4;
                    else if (area <= 500f)
                        kSmall = 1.2;
                    break;
            } // конец вычисления коэффициента для небольших участков

            // Коэффициент к полевым и камеральным работам при обновлении топопланов
            double kUpdate = 1;
            if (checkBoxUpdate.Checked)
                kUpdate = 0.5;

            // Коэффициент к камеральным работам при камеральном создании планов
            // (при этом полевые работы должны отсутствовать!)
            double kCameral = 1;
            if (checkBoxCameral.Checked)
                kCameral = 1.3;

            // Коэффициент к полевым и камеральным работам при составлении плана подземных коммуникаций
            double kUnderground = 1;
            if (checkBoxUnderground.Checked)
            {
                switch (price.Territory)
                {
                    case 0:
                        kUnderground = 1.2;
                        break;
                    case 1:
                        kUnderground = 1.55;
                        break;
                    case 2:
                        kUnderground = 1.75;
                        break;
                }
            } // конец вычисления коэффициента при составлении плана подземных коммуникаций

            // Коэффициент к камеральным работам при нанесении красных линий
            double kRedLines = 1;
            if (checkBoxRedLines.Checked)
            {
                if (checkBoxRedLinesAnalytic.Checked)
                    kRedLines = 1.3;
                else
                    kRedLines = 1.15;
            } // конец вычисления коэффициента к камеральным работам при нанесении красных линий
            
            // Коэффициенты к полевым и камеральным работам при выполнении только вертикальной съемки
            double kVerticalF = 1, kVerticalC = 1;
            if (checkBoxVertical.Checked)
            {
                if (price.Territory == 1)
                {
                    switch (price.Scale)
                    {
                        case 500:
                            kVerticalF = 0.40;
                            kVerticalC = 0.55;
                            break;
                        case 1000:
                            kVerticalF = 0.30;
                            kVerticalC = 0.45;
                            break;
                        default:
                            kVerticalF = 0.25;
                            kVerticalC = 0.40;
                            break;
                    }
                }
                else if (price.Territory == 2)
                {
                    switch (price.Scale)
                    {
                        case 500:
                            kVerticalF = 0.30;
                            kVerticalC = 0.50;
                            break;
                        default:
                            kVerticalF = 0.25;
                            kVerticalC = 0.40;
                            break;
                    }
                }
            } // конец вычисления коэффициентов при выполнении только вертикальной съемки

            // Стоимость полевых и камеральных работ без коэффициентов п. 8(абвг) Общих указаний
            double field;
            if (checkBoxCameral.Checked)
                field = 0; //когда нет полевых работ
            else
                field = Math.Round(area * price.Field * kSmall * kUpdate * kUnderground * kVerticalF, 2);
            double cameral = Math.Round(area * price.Cameral * kUpdate * kCameral * kUnderground * kRedLines * kVerticalC, 2);

            // конец расчетов, относящихся к созданию инженерно-топографических планов

            // Расчет коэффициентов, предусмотренных п. 8 Общих указаний
            // Коэффициент к полевым работам на территориях со специальным режимом
            double kRegime = 1;
            if (checkBoxRegime.Checked)
                kRegime = 1.25;

            // Коэффициент к полевым работам, выполняемым в неблагоприятный период
            double kWinter = 1;
            if (checkBoxWinter.Checked)
            {
                kWinter = regions[comboBoxRegion.Text].WinterFactor;
            }
            // конец расчета коэффициентов, предусмотренных п. 8 Общих указаний

            // Расходы по внутреннему транспорту
            // Общ. ук., п. 9, т. 4
            // стоимость полевых с коэфф. Общ. ук. п. 8(абвг), а также т. 82, т. 83
            double internalBase = Math.Round(field * kRegime * kWinter, 2);
            string internalDistance = comboBoxIntDist.Text;
            int pInternalZeroBased = comboBoxIntDist.SelectedIndex;
            int pInternal = pInternalZeroBased + 1; // параграф табл. 4 (Общ. ук., п. 9)
            double kInternal; // величина из т. 4 (Общ. ук., п. 9)
            if (internalBase <= 75000)
                kInternal = 0.01 * table4[pInternalZeroBased, 0];
            else if (internalBase <= 150000)
                kInternal = 0.01 * table4[pInternalZeroBased, 1];
            else if (internalBase <= 300000)
                kInternal = 0.01 * table4[pInternalZeroBased, 2];
            else if (internalBase <= 750000)
                kInternal = 0.01 * table4[pInternalZeroBased, 3];
            else
                kInternal = 0.01 * table4[pInternalZeroBased, 4];
            double internalTransport = Math.Round(internalBase * kInternal, 2);
            // конец вычисления расходов по внутреннему транспорту

            // Расходы по внешнему транспорту (предполагается продолжительность полевых работ до 1 мес)
            // Общ. ук., п. 10, т. 5
            double externalBase = internalBase + internalTransport; // стоимость полевых для расчета
            string distance = comboBoxExtDist.Text;
            int pExternal = comboBoxExtDist.SelectedIndex; // параграф табл. 5  (Общ. ук., п. 10)
            double kExternal = 0; // величина из т. 5 (Общ. ук., п. 10)
            switch (pExternal)
            {
                case 0:
                    kExternal = 0;
                    break;
                case 1:
                    kExternal = 0.140;
                    break;
                case 2:
                    kExternal = 0.196;
                    break;
                case 3:
                    kExternal = 0.252;
                    break;
                case 4:
                    kExternal = 0.308;
                    break;
                case 5:
                    kExternal = 0.364;
                    break;
                case 6:
                    kExternal = 0.392;
                    break;
            }
            double externalTransport = Math.Round(externalBase * kExternal, 2);
            // конец вычисления расходов по внешнему транспорту

            // Расходы по организации и ликвидации работ на объекте (общ. ук., п. 13)
            // База для расчета - та же, что и для расходов по внешнему транспорту
            // Из сборника не совсем ясно, стоимость каких работ должна использоваться
            // при определении коэффициентов. Здесь приняты полевые изыскания (база для расчета).
            double kOrgLiq = 1;
            if (externalBase <= 30000)
                kOrgLiq = 2.5;
            else if (externalBase <= 75000)
                kOrgLiq = 2.0;
            else if (externalBase <= 150000)
                kOrgLiq = 1.5;
            double orgLiq = Math.Round(externalBase * 0.06 * kOrgLiq, 2);
            // конец вычисления расходов по организации и ликвидации работ

            // Коэффициенты к стоимости изыскательских работ (Общ. ук., п. 15)
            // Коэффициент к полевым и камеральным работам при выдаче промежуточных материалов заказчику
            double kIntermediate = 1;
            if (checkBoxIntermediate.Checked)
                kIntermediate = 1.1;
            // Коэффициент к камеральным работам с использованием материалов ограниченного пользования
            double kRestricted = 1;
            if (checkBoxRestricted.Checked)
                kRestricted = 1.1;
            // Коэффициент к камеральным работам при составлении плана в цвете (красках)
            double kColour = 1;
            if (checkBoxColour.Checked)
                kColour = 1.1;
            // Коэффициент к камеральным работам с использованием компьютерных технологий
            double kComputer = 1;
            if (checkBoxComputer.Checked)
                kComputer = 1.2;
            // конец определения коэффициентов к стоимости изыскательских работ

            // Коэффициент к полевым работам без выплаты полевого довольствия (Общ. ук., п. 14)
            double kNoFieldAllowances = checkBoxNoFieldAllowances.Checked ? 0.85 : 1;

            // Сметная стоимость изыскательских работ с учетом повышающих коэффициентов
            double totalField = Math.Round(internalBase * kIntermediate * kNoFieldAllowances, 2);
            double totalCameral = Math.Round(cameral * kIntermediate * kRestricted * kColour * kComputer, 2);

            // Непредвиденные расходы (Общ. ук., п. 18)
            double unforeseenBase = totalField + totalCameral +
                internalTransport + externalTransport + orgLiq;
            double unforeseenPercent = checkBoxUnforeseen.Checked ?
                (double)numericUpDownUnforeseen.Value : 0; // Вообще-то должно быть не менее 10%
            double unforeseen = Math.Round(unforeseenBase * unforeseenPercent / 100.0, 2);
            double withUnforeseen = unforeseenBase + unforeseen;

            // Повышающий коэффициент за срочное выполнение (Общ. ук., п. 19)
            double urgencyFactor = checkBoxUrgency.Checked ?
                (double)numericUpDownUrgency.Value : 1;

            // Повышающий коэффициент на основе районного коэффициента к заработной плате
            double salaryBasedFactor = checkBoxSalaryFactor.Checked ?
                SalaryFactorToPriceFactor((double)comboBoxSalaryFactor.SelectedItem) : 1;

            // Регистрация изыскательских работ и приемка материалов выполненных инженерных изысканий
            // Гл. 9, п. 5, т. 80
            double registrationBase = Math.Round(withUnforeseen * urgencyFactor * salaryBasedFactor, 2);
            double dReg = 0, cReg = 0, kReg = 0; //цена считается как cReg + kReg * (registrationBase - dReg)
            int pReg = 0; // параграф т. 80
            if (checkBoxRegistration.Checked)
            {
                if (registrationBase <= 25000)
                {
                    dReg = 0;
                    cReg = 0;
                    kReg = 0.04;
                    pReg = 1;
                }
                else if (registrationBase <= 50000)
                {
                    dReg = 25000;
                    cReg = 1000;
                    kReg = 0.03;
                    pReg = 2;
                }
                else if (registrationBase <= 100000)
                {
                    dReg = 50000;
                    cReg = 1750;
                    kReg = 0.025;
                    pReg = 3;
                }
                else if (registrationBase <= 500000)
                {
                    dReg = 100000;
                    cReg = 3250;
                    kReg = 0.02;
                    pReg = 4;
                }
                else if (registrationBase <= 1000000)
                {
                    dReg = 500000;
                    cReg = 11250;
                    kReg = 0.015;
                    pReg = 5;
                }
                else if (registrationBase <= 2000000)
                {
                    dReg = 1000000;
                    cReg = 18750;
                    kReg = 0.01;
                    pReg = 6;
                }
                else if (registrationBase <= 5000000)
                {
                    dReg = 2000000;
                    cReg = 28750;
                    kReg = 0.005;
                    pReg = 7;
                }
                else if (registrationBase <= 10000000)
                {
                    dReg = 5000000;
                    cReg = 43750;
                    kReg = 0.0025;
                    pReg = 8;
                }
                else
                {
                    dReg = 10000000;
                    cReg = 56250;
                    kReg = 0.001;
                    pReg = 9;
                }
            }
            double registration = Math.Round(cReg + kReg * (registrationBase - dReg), 2);
            // конец расчета стоимости регистрации и приемки изыскательских работ

            // Стоимость услуг архивных фондов (гл. 9, п.6, т. 81)
            // Выдача во временное пользование материалов топографических съемок
            double materials = 235 * (int)numericUpDownMaterials.Value;
            // Выдача координат пунктов геодезической сети, сети сгущения (съемочной сети)
            double coordinates = 80 * (int)numericUpDownCoordinates.Value;
            // Выдача высот пунктов (знаков) геодезических и нивелирных сетей, сетей сгущения (съемочных сетей)
            double heights = 80 * (int)numericUpDownHeights.Value;
            // конец расчета стоимости услуг архивных фондов

            // Итог в ценах на 01.01.2001
            double total = registrationBase + registration +
                materials + coordinates + heights;

            // Итог с учетом коэффициента инфляции
            double kInflation = (double)numericUpDownInflation.Value;
            string inflationReason = textBoxInflationReason.Text;
            double actual = Math.Round(kInflation * total, 2);

            // НДС
            double vatPercent = checkBoxVAT.Checked ? (double)numericUpDownVAT.Value : 0;
            vat = Math.Round(actual * vatPercent / 100.0, 2);

            // Итог с НДС
            finalTotal = actual + vat;

            // Заполнение строк сметы
            estimate.Clear();
            estimate.Add(new EstimateLine("№ п/п", "Обоснование", "Наименование работ",
                "Ед. изм.", "Количество", "Цена ед., руб", "Стоимость, руб."));
            estimate.Add(new EstimateLine("Создание инженерно-топографических планов " +
                (price.Territory == 0 ? "незастроенной территории " :
                price.Territory == 1 ? "застроенной территории " :
                "территории действующих промышленных предприятий ") +
                price.Category.ToString() + " категории сложности масштаба 1: " +
                price.Scale.ToString() + " с высотой сечения рельефа " +
                price.Height.ToString() + " м"));
            if (field != 0)
                estimate.Add(new EstimateLine("табл. 9, \u00a7 " + price.Paragraph.ToString(),
                    "Полевые работы", "га", area.ToString(), price.Field.ToString(),
                    (area * price.Field).ToString("0.00")));
            estimate.Add(new EstimateLine("табл. 9, \u00a7 " + price.Paragraph.ToString(),
                "Камеральные работы", "га", area.ToString(), price.Cameral.ToString(),
                (area * price.Cameral).ToString("0.00")));
            if(kSmall != 1 && field != 0)
                estimate.Add(new EstimateLine("гл. 2, п. 6",
                    "Съемка небольших участков или узких полос", "коэфф.",
                    "к стоимости полевых работ", kSmall.ToString(), ""));
            if (kUpdate != 1)
                estimate.Add(new EstimateLine("табл. 9, прим. 3",
                    "Обновление инженерно-топографических планов", "коэфф.",
                    "к стоимости изыскательских работ", kUpdate.ToString(), ""));
            if (kCameral != 1)
                estimate.Add(new EstimateLine("гл. 2, п. 7",
                    "Составление инженерно-топографических планов по существующим материалам " +
                    "без выполнения полевых работ", "коэфф.",
                    "к стоимости камеральных работ", kCameral.ToString(), ""));
            if (kUnderground != 1)
                estimate.Add(new EstimateLine("табл. 9, прим. 4",
                    "Съемка подземных коммуникаций с помощью приборов поиска " +
                    "и составление плана подземных коммуникаций", "коэфф.",
                    "к стоимости изыскательских работ", kUnderground.ToString(), ""));
            if (kRedLines == 1.15)
                estimate.Add(new EstimateLine("табл. 9, прим. 6",
                    "Нанесение красных линий или линий регулирования застройки ", "коэфф.",
                    "к стоимости камеральных работ", kRedLines.ToString(), ""));
            else if (kRedLines == 1.3)
                estimate.Add(new EstimateLine("табл. 9, прим. 6",
                    "Нанесение красных линий или линий регулирования застройки " +
                    "с предварительным аналитическим расчётом их координат", "коэфф.",
                    "к стоимости камеральных работ", kRedLines.ToString(), ""));
            if (kVerticalF != 1 && field != 0)
                estimate.Add(new EstimateLine("гл. 2, п. 4",
                    "Выполнение вертикальной (высотной) съемки " +
                    "на планах горизонтальной съемки", "коэфф.",
                    "к стоимости полевых работ", kVerticalF.ToString(), ""));
            if (kVerticalC != 1)
                estimate.Add(new EstimateLine("гл. 2, п. 4",
                    "Выполнение вертикальной (высотной) съемки " +
                    "на планах горизонтальной съемки", "коэфф.",
                    "к стоимости камеральных работ", kVerticalC.ToString(), ""));
            if (kRegime != 1 && field != 0)
                estimate.Add(new EstimateLine("общ. ук., п. 8в",
                    "Выполнение изысканий на территориях со специальным режимом", "коэфф.",
                    "к стоимости полевых работ", kRegime.ToString(), ""));
            if (kWinter != 1 && field != 0)
                estimate.Add(new EstimateLine("общ. ук., п. 8г",
                    "Выполнение полевых изыскательских работ в неблагоприятный период года",
                    "коэфф.", "к стоимости полевых работ", kWinter.ToString(), ""));
            if(field != 0)
                estimate.Add(new EstimateLine("", "Итого полевых работ",
                    "", "", "", internalBase.ToString("0.00")));
            estimate.Add(new EstimateLine("", "Итого камеральных работ",
                "", "", "", cameral.ToString("0.00")));
            estimate.Add(new EstimateLine("Расходы, предусмотренные Общими указаниями"));
            if (internalTransport != 0)
                estimate.Add(new EstimateLine("табл. 4, \u00a7 " + pInternal.ToString(),
                    "Расходы по внутреннему транспорту, " +
                    "расстояние от базы до участка изысканий " + internalDistance + " км", "",
                    "% от стоимости полевых работ", (kInternal * 100).ToString() + "%",
                    internalTransport.ToString("0.00")));
            if (externalTransport != 0)
                estimate.Add(new EstimateLine("табл. 5, \u00a7 " + pExternal.ToString(),
                    "Расходы по внешнему транспорту, расстояние от базы до участка изысканий " +
                    distance + " км, продолжительность работ до 1 мес.", "",
                    "% от стоимости полевых работ, включая внутренний транспорт",
                    (kExternal * 100).ToString() + "%",
                    externalTransport.ToString("0.00")));
            if (orgLiq != 0)
                estimate.Add(new EstimateLine("общ. ук., п. 13",
                    "Расходы по организации и ликвидации работ на объекте", "",
                    "6% от стоимости полевых работ, включая внутренний транспорт" +
                    (kOrgLiq == 1 ? "" : ", с применением коэффициента"),
                    kOrgLiq == 1 ? "" : kOrgLiq.ToString(), orgLiq.ToString("0.00")));
            if (kNoFieldAllowances != 1 && field != 0)
                estimate.Add(new EstimateLine("общ. ук., п. 14",
                    "Проведение полевых работ без выплаты работникам полевого довольствия или командировочных",
                    "коэфф.", "к стоимости полевых работ", kNoFieldAllowances.ToString(), ""));
            if (kIntermediate != 1)
                estimate.Add(new EstimateLine("общ. ук., п. 15а",
                    "Выдача заказчику промежуточных материалов изысканий", "коэфф.",
                    "к стоимости изыскательских работ", kIntermediate.ToString(), ""));
            if (kRestricted != 1)
                estimate.Add(new EstimateLine("общ. ук., п. 15б",
                    "Выполнение камеральных работ с использованием материалов ограниченного пользования",
                    "коэфф.", "к стоимости камеральных работ", kRestricted.ToString(), ""));
            if (kColour != 1)
                estimate.Add(new EstimateLine("общ. ук., п. 15г",
                    "Составление плана подземных и надземных сооружений в цвете (красках)",
                    "коэфф.", "к стоимости камеральных работ", kColour.ToString(), ""));
            if (kComputer != 1)
                estimate.Add(new EstimateLine("общ. ук., п. 15д",
                    "Выполнение камеральных и картографических работ с применением компьютерных технологий",
                    "коэфф.", "к стоимости камеральных работ", kComputer.ToString(), ""));
            if (field != 0)
                estimate.Add(new EstimateLine("Итого полевых работ на объекте с учетом коэффициентов",
                    totalField.ToString("0.00")));
            estimate.Add(new EstimateLine("Итого камеральных работ на объекте с учетом коэффициентов",
                totalCameral.ToString("0.00")));
            estimate.Add(new EstimateLine("Итого сметная стоимость, включая транспортные " +
                "и организационно-ликвидационные расходы", unforeseenBase.ToString("0.00")));
            if (unforeseenPercent != 0)
            {
                estimate.Add(new EstimateLine("общ. ук., п. 18", "Непредвиденные расходы", "",
                    "% от сметной стоимости", unforeseenPercent.ToString() + "%",
                    unforeseen.ToString("0.00")));
                estimate.Add(new EstimateLine("Итого с непредвиденными расходами",
                    withUnforeseen.ToString("0.00")));
            }
            if (urgencyFactor != 1)
            {
                estimate.Add(new EstimateLine("общ. ук., п. 19", "Повышающий коэффициент за срочное выполнение",
                    "коэфф.", "к сметной стоимости", urgencyFactor.ToString(), ""));
            }
            if (salaryBasedFactor != 1)
            {
                estimate.Add(new EstimateLine("общ. ук., п. 8д", "Повышающий коэффициент на основе регионального коэффициента к заработной плате",
                    "коэфф.", "к сметной стоимости", salaryBasedFactor.ToString(), ""));
            }
            if (urgencyFactor != 1 || salaryBasedFactor != 1)
            {
                estimate.Add(new EstimateLine("Итого с повышающими коэффициентами к итогу",
                    registrationBase.ToString("0.00")));
            }
            if (registration + materials + coordinates + heights != 0)
                estimate.Add(new EstimateLine("Регистрация инженерных изысканий и услуги архивных фондов"));
            if (registration != 0)
                estimate.Add(new EstimateLine("табл. 80, \u00a7 " + pReg.ToString(),
                    "Регистрация изыскательских работ и приемка материалов выполненных инженерных изысканий",
                    "", (cReg == 0 ? "" : cReg.ToString() + " + ") + (kReg * 100).ToString() +
                    "% от стоимости изысканий" + (dReg == 0 ? "" : " свыше " + dReg.ToString()+ " руб."),
                    "", registration.ToString("0.00")));
            if (materials != 0)
                estimate.Add(new EstimateLine("табл. 81, \u00a7 1",
                    "Выдача во временное пользование материалов топографических съемок",
                    "планшет, трапеция", Math.Round(materials / 235.0).ToString(),
                    "235", materials.ToString("0.00")));
            if (coordinates != 0)
                estimate.Add(new EstimateLine("табл. 81, \u00a7 2",
                    "Выдача координат пунктов геодезической сети, сети сгущения (съемочной сети)",
                    "пункт", Math.Round(coordinates / 80.0).ToString(),
                    "80", coordinates.ToString("0.00")));
            if (heights != 0)
                estimate.Add(new EstimateLine("табл. 81, \u00a7 3",
                    "Выдача высот пунктов (знаков) геодезических и нивелирных сетей, " +
                    "сетей сгущения (съемочных сетей)", "пункт", Math.Round(heights / 80.0).ToString(),
                    "80", heights.ToString("0.00")));
            if (registration + materials + coordinates + heights != 0)
                estimate.Add(new EstimateLine("Итого по смете", total.ToString("0.00")));
            estimate.Add(new EstimateLine(inflationReason, "Коэффициент инфляции",
                "", "", kInflation.ToString("0.00"),""));
            estimate.Add(new EstimateLine("Итого с учетом коэффициента инфляции",
                Math.Round(actual, 2).ToString("0.00")));
            if (vat != 0)
            {
                estimate.Add(new EstimateLine("", "НДС",
                "", "", vatPercent.ToString() + "%", Math.Round(vat, 2).ToString("0.00")));
                estimate.Add(new EstimateLine("Итого с НДС",
                    Math.Round(finalTotal, 2).ToString("0.00")));
            }

            // конец заполнения строк сметы
            // Нумеруем строки сметы
            int lineNumber = 0;
            for (int i = 0; i < estimate.Count; i++)
            {
                if(estimate[i].N == "" && estimate[i].Reason != "")
                {
                    // Такие сложности, потому что структуру в составе списка изменить невозможно
                    EstimateLine newEstimateLine = estimate[i];
                    newEstimateLine.N = (++lineNumber).ToString();
                    estimate[i] = newEstimateLine;
                }
            }

            // Выводим диалог просмотра перед печатью
            try
            {
                printPreviewDialog1.ShowDialog(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при просмотре и печати сметы может быть вызвана " +
                    "отсутствием установленных принтеров или неправильной работой драйвера принтера.\n" +
                    "Информация об ошибке:\n" + ex.Message, "Ошибка при просмотре или печати сметы",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Отображает шапку сметы
        /// </summary>
        private void drawEstimateHeader(System.Drawing.Printing.PrintPageEventArgs e, float q, ref float position)
        {
            Graphics g = e.Graphics;
            Rectangle bounds = e.MarginBounds; // Границы области в пределах полей
            Font font = new Font("Arial", 7, FontStyle.Regular); // обычный шрифт
            Font hfont = new Font("Arial", 10, FontStyle.Bold); // шрифт для шапки сметы
            StringFormat sfRight = new StringFormat(); // Для ячеек, выравниваемых вправо
            sfRight.Alignment = StringAlignment.Far;
            StringFormat sfHeader = new StringFormat(); // Используется при выводе первой строки
            sfHeader.Alignment = StringAlignment.Center;
            sfHeader.LineAlignment = StringAlignment.Center;
            float s = position; // Вертикальная позиция текущего выводимого элемента

            // Отображение шапки сметы
            string contractNumber = string.IsNullOrEmpty(textBoxContractNumber.Text) ?
                "__________" : textBoxContractNumber.Text;
            string contractDate = dateTimePickerContractDate.Checked ?
                dateTimePickerContractDate.Value.ToString("d") : "______________";
            string objectName = string.IsNullOrEmpty(textBoxObjectName.Text) ?
                new string('_', 83) : textBoxObjectName.Text;
            string customer = string.IsNullOrEmpty(textBoxCustomer.Text) ?
                new string('_', 82) : textBoxCustomer.Text;
            string contractor = string.IsNullOrEmpty(textBoxContractor.Text) ?
                new string('_', 80) : textBoxContractor.Text;
            g.DrawString("Приложение к договору № " + contractNumber + " от " + contractDate,
                font, Brushes.Black, new RectangleF(q, s, bounds.Right - q, 15), sfRight);
            s += 25;
            g.DrawString("СМЕТА", hfont, Brushes.Black, new RectangleF(q, s, bounds.Width, 20), sfHeader);
            s += 15;
            g.DrawString("на выполнение комплекса топографо-геодезических работ",
                hfont, Brushes.Black, new RectangleF(q, s, bounds.Width, 15), sfHeader);
            s += 20;
            g.DrawString("Наименование объекта:  " + objectName,
               font, Brushes.Black, new RectangleF(q, s, bounds.Width, 15));
            s += 15;
            g.DrawString("Наименование заказчика:  " + customer,
                font, Brushes.Black, new RectangleF(q, s, bounds.Width, 15));
            s += 15;
            g.DrawString("Наименование подрядчика:  " + contractor,
                font, Brushes.Black, new RectangleF(q, s, bounds.Width, 15));
            s += 15;
            g.DrawString("Сметный расчет составлен по " +
                "\u00abСправочнику базовых цен на инженерные изыскания для строительства\u00bb 2004 г.",
                font, Brushes.Black, new RectangleF(q, s, bounds.Width, 15));
            s += 15;
            position = s;
            nextRow = 0;
            e.HasMorePages = true;
        }

        /// <summary>
        /// Отображает таблицу сметы
        /// </summary>
        private void drawEstimateTable(System.Drawing.Printing.PrintPageEventArgs e, float q, ref float position)
        {
            if(nextRow < 0 || nextRow >= estimate.Count)
                return;
            Graphics g = e.Graphics;
            Rectangle bounds = e.MarginBounds; // Границы области в пределах полей
            int u = (int)((float)(bounds.Width) / 26); // Модуль для расчета ширины ячеек
            Font font = new Font("Arial", 7, FontStyle.Regular); // обычный шрифт
            Font bfont = new Font("Arial", 7, FontStyle.Bold); // жирный шрифт
            StringFormat sfRight = new StringFormat(); // Для ячеек, выравниваемых вправо
            sfRight.Alignment = StringAlignment.Far;
            StringFormat sfCenter = new StringFormat(); // Для ячеек, выравниваемых по центру
            sfCenter.Alignment = StringAlignment.Center;
            StringFormat sfHeader = new StringFormat(); // Используется при выводе подзаголовков
            sfHeader.Alignment = StringAlignment.Center;
            sfHeader.LineAlignment = StringAlignment.Center;
            float s = position; // Вертикальная позиция текущего выводимого элемента
            
            // Отображение границ и содержимого ячеек таблицы
            int j;
            for(j = nextRow; j < estimate.Count; j++)
            {
                EstimateLine line = estimate[j];
                float h = 0;
                if (line.isHeader) // Если текущая строка - подзаголовок таблицы
                {
                    h = g.MeasureString(line.N, bfont, 26 * u).Height; // Определяем высоту строки (ячейки)
                    if (s + h > bounds.Bottom)
                        break;
                    g.DrawString(line.N, bfont, Brushes.Black, new RectangleF(q, s, 26 * u, h));
                    g.DrawRectangle(Pens.Black, q, s, 26 * u, h);
                }
                else if (line.isTotal) // Если текущая строка - промежуточный или конечный итог таблицы
                {
                    float h1 = g.MeasureString(line.N, bfont, 23 * u).Height;
                    float h2 = g.MeasureString(line.Cost, bfont, 3 * u).Height;
                    h = Math.Max(h1, h2); // Высота строки равняется наибольшему значению
                    if (s + h > bounds.Bottom)
                        break;
                    g.DrawString(line.N, bfont, Brushes.Black, new RectangleF(q, s, 23 * u, h));
                    g.DrawRectangle(Pens.Black, q, s, 23 * u, h);
                    g.DrawString(line.Cost, bfont, Brushes.Black,
                        new RectangleF(q + 23 * u, s, 3 * u, h), sfRight);
                    g.DrawRectangle(Pens.Black, q + 23 * u, s, 3 * u, h);
                }
                else if (estimate.IndexOf(line) == 0) // Если текущая строка - первая, то есть заголовок
                {
                    // Определяем высоту строки как высоту самой высокой ячейки
                    float[] cellHeights = new float[7];
                    cellHeights[0] = g.MeasureString(line.N, bfont, u, sfHeader).Height;
                    cellHeights[1] = g.MeasureString(line.Reason, bfont, 4 * u, sfHeader).Height;
                    cellHeights[2] = g.MeasureString(line.Work, bfont, 10 * u, sfHeader).Height;
                    cellHeights[3] = g.MeasureString(line.Unit, bfont, 2 * u, sfHeader).Height;
                    cellHeights[4] = g.MeasureString(line.Count, bfont, 4 * u, sfHeader).Height;
                    cellHeights[5] = g.MeasureString(line.Price, bfont, 2 * u, sfHeader).Height;
                    cellHeights[6] = g.MeasureString(line.Cost, bfont, 3 * u, sfHeader).Height;
                    foreach (float ch in cellHeights)
                        if (ch > h) h = ch;
                    if (s + h > bounds.Bottom)
                        break;
                    // Выводим ячейки по очереди
                    g.DrawString(line.N, bfont, Brushes.Black, new RectangleF(q, s, u, h), sfHeader);
                    g.DrawRectangle(Pens.Black, q, s, u, h);
                    g.DrawString(line.Reason, bfont, Brushes.Black,
                        new RectangleF(q + u, s, 4 * u, h), sfHeader);
                    g.DrawRectangle(Pens.Black, q + u, s, 4 * u, h);
                    g.DrawString(line.Work, bfont, Brushes.Black,
                        new RectangleF(q + 5 * u, s, 10 * u, h), sfHeader);
                    g.DrawRectangle(Pens.Black, q + 5 * u, s, 10 * u, h);
                    g.DrawString(line.Unit, bfont, Brushes.Black,
                        new RectangleF(q + 15 * u, s, 2 * u, h), sfHeader);
                    g.DrawRectangle(Pens.Black, q + 15 * u, s, 2 * u, h);
                    g.DrawString(line.Count, bfont, Brushes.Black,
                        new RectangleF(q + 17 * u, s, 4 * u, h), sfHeader);
                    g.DrawRectangle(Pens.Black, q + 17 * u, s, 4 * u, h);
                    g.DrawString(line.Price, bfont, Brushes.Black,
                        new RectangleF(q + 21 * u, s, 2 * u, h), sfHeader);
                    g.DrawRectangle(Pens.Black, q + 21 * u, s, 2 * u, h);
                    g.DrawString(line.Cost, bfont, Brushes.Black,
                        new RectangleF(q + 23 * u, s, 3 * u, h), sfHeader);
                    g.DrawRectangle(Pens.Black, q + 23 * u, s, 3 * u, h);
                }
                else // Если текущая строка - обычная, не содержащая объединений
                {
                    // Определяем высоту строки как высоту самой высокой ячейки
                    float[] cellHeights = new float[7];
                    cellHeights[0] = g.MeasureString(line.N, font, u, sfCenter).Height;
                    cellHeights[1] = g.MeasureString(line.Reason, font, 4 * u).Height;
                    cellHeights[2] = g.MeasureString(line.Work, font, 10 * u).Height;
                    cellHeights[3] = g.MeasureString(line.Unit, font, 2 * u, sfCenter).Height;
                    cellHeights[4] = g.MeasureString(line.Count, font, 4 * u, sfCenter).Height;
                    cellHeights[5] = g.MeasureString(line.Price, font, 2 * u, sfCenter).Height;
                    cellHeights[6] = g.MeasureString(line.Cost, font, 3 * u, sfRight).Height;
                    foreach (float ch in cellHeights)
                        if (ch > h) h = ch;
                    if (s + h > bounds.Bottom)
                        break;
                    // Выводим ячейки по очереди
                    g.DrawString(line.N, font, Brushes.Black, new RectangleF(q, s, u, h), sfCenter);
                    g.DrawRectangle(Pens.Black, q, s, u, h);
                    g.DrawString(line.Reason, font, Brushes.Black, new RectangleF(q + u, s, 4 * u, h));
                    g.DrawRectangle(Pens.Black, q + u, s, 4 * u, h);
                    g.DrawString(line.Work, font, Brushes.Black, new RectangleF(q + 5 * u, s, 10 * u, h));
                    g.DrawRectangle(Pens.Black, q + 5 * u, s, 10 * u, h);
                    g.DrawString(line.Unit, font, Brushes.Black,
                        new RectangleF(q + 15 * u, s, 2 * u, h), sfCenter);
                    g.DrawRectangle(Pens.Black, q + 15 * u, s, 2 * u, h);
                    g.DrawString(line.Count, font, Brushes.Black,
                        new RectangleF(q + 17 * u, s, 4 * u, h), sfCenter);
                    g.DrawRectangle(Pens.Black, q + 17 * u, s, 4 * u, h);
                    g.DrawString(line.Price, font, Brushes.Black,
                        new RectangleF(q + 21 * u, s, 2 * u, h), sfCenter);
                    g.DrawRectangle(Pens.Black, q + 21 * u, s, 2 * u, h);
                    g.DrawString(line.Cost, font, Brushes.Black,
                        new RectangleF(q + 23 * u, s, 3 * u, h), sfRight);
                    g.DrawRectangle(Pens.Black, q + 23 * u, s, 3 * u, h);
                }
                s += h; // Смещаемся вниз на высоту выведенной строки
            }

            // Вывод рекламной строки
            drawAdvertisementString(e, q, s);

            if (j < estimate.Count) // Таблица выведена не полностью, продолжение - на следующей странице
            {
                e.HasMorePages = true;
            }
            else // Иначе будет выведен подвал сметы
            {
                position = s;
            }
            nextRow = j;
        }

        /// <summary>
        /// Вывод рекламной строки по левому краю сметы
        /// </summary>
        /// <param name="g">Объект Graphics, используемый для рисования</param>
        /// <param name="bounds">Прямоугольник, в котором осуществляется рисование страницы</param>
        /// <param name="position">Вертикальная позиция следующего выводимого элемента</param>
        private void drawAdvertisementString(System.Drawing.Printing.PrintPageEventArgs e, float x, float y)
        {
            Graphics g = e.Graphics;
            Rectangle bounds = e.MarginBounds; // Границы области в пределах полей
            Font afont = new Font("Arial", 5, FontStyle.Regular); // шрифт для рекламной строки
            
            // Выбираем рекламную строку, подходящую по длине
            float length;
            string adsText = "Подготовлено с использованием программы \u00abТопосмета\u00bb";
            length = g.MeasureString(adsText, afont).Width;
            if (y - length < bounds.Top)
            {
                adsText = "Программа \u00abТопосмета\u00bb";
                length = g.MeasureString(adsText, afont).Width;
                if (y - length < bounds.Top)
                    adsText = "";
            }

            GraphicsState gState = g.Save();
            g.TranslateTransform(x - 10, y);
            g.RotateTransform(270);
            g.DrawString(adsText, afont, Brushes.Black, new PointF(0, 0));
            g.Restore(gState);
        }

        /// <summary>
        /// Отображает "подвал" сметы
        /// </summary>
        /// <param name="g">Объект Graphics, используемый для рисования</param>
        /// <param name="bounds">Прямоугольник, в котором осуществляется рисование</param>
        /// <param name="position">Вертикальная позиция следующего выводимого элемента</param>
        private void drawEstimateFooter(System.Drawing.Printing.PrintPageEventArgs e, float q, ref float position)
        {
            Graphics g = e.Graphics;
            Rectangle bounds = e.MarginBounds; // Границы области в пределах полей
            float s = position; // Вертикальная позиция текущего выводимого элемента
            Font font = new Font("Arial", 7, FontStyle.Regular); // Обычный шрифт

            // Если "подвал" не помещается на текущей странице, переносим на следующую
            if (s > bounds.Bottom - 130 && s != bounds.Top)
            {
                e.HasMorePages = true;
                return;
            }
            // Отображение "подвала" сметы
            string totalString = "Итого по смете " + Math.Round(finalTotal, 2).ToString("0.00") + " руб. (" +
                spelledValue(finalTotal) + ")";
            if (vat != 0)
                totalString += ", в том числе НДС " + Math.Round(vat, 2).ToString("0.00") + " руб.";
            s += 10;
            g.DrawString(totalString, font, Brushes.Black, new RectangleF(q, s, bounds.Width, 30));
            s += 30;
            g.DrawString(new string('_', 40),
                font, Brushes.Black, new RectangleF(q, s, 0.45f * bounds.Width, 30));
            g.DrawString(new string('_', 40),
                font, Brushes.Black, new RectangleF(q + 0.5f * bounds.Width, s, 0.5f * bounds.Width, 30));
            s += 30;
            g.DrawString(new string('_', 40),
                font, Brushes.Black, new RectangleF(q, s, 0.45f * bounds.Width, 30));
            g.DrawString(new string('_', 40),
                font, Brushes.Black, new RectangleF(q + 0.5f * bounds.Width, s, 0.5f * bounds.Width, 30));
            s += 30;
            g.DrawString("____ ______________ ________ г.",
                font, Brushes.Black, new RectangleF(q, s, 0.45f * bounds.Width, 30));
            g.DrawString("____ ______________ ________ г.",
                font, Brushes.Black, new RectangleF(q + 0.5f * bounds.Width, s, 0.5f * bounds.Width, 30));
            s += 30;
            position = s;
            e.HasMorePages = false;
        }

        /// <summary>
        /// Перед началом печати выполняется разбивка на страницы
        /// </summary>
        private void printDocument1_BeginPrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            nextRow = -1; // Первой будет выводиться шапка сметы
        }

        /// <summary>
        /// Выводит страницу на печать
        /// </summary>
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int u = (int)((float)(e.MarginBounds.Width) / 26); // Модуль для расчета ширины ячеек
            float q = e.MarginBounds.Left + e.MarginBounds.Width - 26 * u; // Левая граница отображаемой области
            float s = e.MarginBounds.Top; // Вертикальная позиция текущего выводимого элемента
            
            if(nextRow == -1)
                drawEstimateHeader(e, q, ref s);
            if(nextRow >= 0 && nextRow < estimate.Count)
                drawEstimateTable(e, q, ref s);
            if(nextRow >= estimate.Count)
                drawEstimateFooter(e, q, ref s);
        }

        // Возвращает сумму в рублях прописью
        private string spelledValue(double n)
        {
            long i = (long)(Math.Floor(n));
            int k = (int)(Math.Round(100 *(n - i),MidpointRounding.ToEven));
            StringBuilder sb = new StringBuilder(256);
            if (i == 0)
            {
                sb.Append("ноль рублей ");
            }
            else
            {
                int units = (int)(i % 1000);
                int thousands = (int)(i / 1000 % 1000);
                int millions = (int)(i / 1000000 % 1000);
                int billions = (int)(i / 1000000000 % 1000);
                int trillions = (int)(i / 1000000000000 % 1000);
                string sv;
                int cs = 0; // Падеж не раз понадобится в дальнейшем
                if (trillions != 0)
                {
                    spelledSmallValue(trillions, true, out sv, out cs);
                    sb.Append(sv);
                    switch (cs)
                    {
                        case 0:
                            sb.Append("триллионов ");
                            break;
                        case 1:
                            sb.Append("триллион ");
                            break;
                        case 2:
                            sb.Append("триллиона ");
                            break;
                    }
                }
                if (billions != 0)
                {
                    spelledSmallValue(billions, true, out sv, out cs);
                    sb.Append(sv);
                    switch (cs)
                    {
                        case 0:
                            sb.Append("миллиардов ");
                            break;
                        case 1:
                            sb.Append("миллиард ");
                            break;
                        case 2:
                            sb.Append("миллиарда ");
                            break;
                    }
                }
                if (millions != 0)
                {
                    spelledSmallValue(millions, true, out sv, out cs);
                    sb.Append(sv);
                    switch (cs)
                    {
                        case 0:
                            sb.Append("миллионов ");
                            break;
                        case 1:
                            sb.Append("миллион ");
                            break;
                        case 2:
                            sb.Append("миллиона ");
                            break;
                    }
                }
                if (thousands != 0)
                {
                    spelledSmallValue(thousands, false, out sv, out cs);
                    sb.Append(sv);
                    switch (cs)
                    {
                        case 0:
                            sb.Append("тысяч ");
                            break;
                        case 1:
                            sb.Append("тысяча ");
                            break;
                        case 2:
                            sb.Append("тысячи ");
                            break;
                    }
                }
                if (units != 0)
                {
                    spelledSmallValue(units, true, out sv, out cs);
                    sb.Append(sv);
                }
                // Добавляем слово "рубль" в нужном числе и падеже
                switch (cs)
                {
                    case 0:
                        sb.Append("рублей ");
                        break;
                    case 1:
                        sb.Append("рубль ");
                        break;
                    case 2:
                        sb.Append("рубля ");
                        break;
                }
            }
            // Копейки выводим в виде двузначного числа
            sb.Append(k.ToString("00"));
            if(k >= 6 && k <= 20)
                sb.Append(" копеек");
            else
                switch (k % 10)
                {
                    case 1:
                        sb.Append(" копейка");
                        break;
                    case 2:
                    case 3:
                    case 4:
                        sb.Append(" копейки");
                        break;
                    default:
                        sb.Append(" копеек");
                        break;
                }
            // Первую букву делаем заглавной
            sb[0] = char.ToUpper(sb[0]);
            return sb.ToString();
        }

        // Вспомогательная функция. Возвращает число от 1 до 999 прописью и соответствующий падеж
        // в зависимости от указанного рода (true = мужской, false = женский) с пробелом на конце.
        // Падежи закодированы так: 0 = родит. мн. ч., 1 = именит. ед. ч., 2 = родит. ед. ч. 
        private void spelledSmallValue(int n, bool sex, out string s, out int c)
        {
            c = 0;
            StringBuilder sb = new StringBuilder(32);
            switch (n / 100)
            {
                case 1:
                    sb.Append("сто ");
                    break;
                case 2:
                    sb.Append("двести ");
                    break;
                case 3:
                    sb.Append("триста ");
                    break;
                case 4:
                    sb.Append("четыреста ");
                    break;
                case 5:
                    sb.Append("пятьсот ");
                    break;
                case 6:
                    sb.Append("шестьсот ");
                    break;
                case 7:
                    sb.Append("семьсот ");
                    break;
                case 8:
                    sb.Append("восемьсот ");
                    break;
                case 9:
                    sb.Append("девятьсот ");
                    break;
            }
            bool teen = false;
            switch (n / 10 % 10)
            {
                case 1:
                    teen = true;
                    break;
                case 2:
                    sb.Append("двадцать ");
                    break;
                case 3:
                    sb.Append("тридцать ");
                    break;
                case 4:
                    sb.Append("сорок ");
                    break;
                case 5:
                    sb.Append("пятьдесят ");
                    break;
                case 6:
                    sb.Append("шестьдесят ");
                    break;
                case 7:
                    sb.Append("семьдесят ");
                    break;
                case 8:
                    sb.Append("восемьдесят ");
                    break;
                case 9:
                    sb.Append("девяносто ");
                    break;
            }
            if (teen)
            {
                switch (n % 10)
                {
                    case 0:
                        sb.Append("десять ");
                        break;
                    case 1:
                        sb.Append("одиннадцать ");
                        break;
                    case 2:
                        sb.Append("двенадцать ");
                        break;
                    case 3:
                        sb.Append("тринадцать ");
                        break;
                    case 4:
                        sb.Append("четырнадцать ");
                        break;
                    case 5:
                        sb.Append("пятнадцать ");
                        break;
                    case 6:
                        sb.Append("шестнадцать ");
                        break;
                    case 7:
                        sb.Append("семнадцать ");
                        break;
                    case 8:
                        sb.Append("восемнадцать ");
                        break;
                    case 9:
                        sb.Append("девятнадцать ");
                        break;
                }
            }
            else
            {
                switch (n % 10)
                {
                    case 1:
                        if (sex)
                            sb.Append("один ");
                        else
                            sb.Append("одна ");
                        c = 1;
                        break;
                    case 2:
                        if (sex)
                            sb.Append("два ");
                        else
                            sb.Append("две ");
                        c = 2;
                        break;
                    case 3:
                        sb.Append("три ");
                        c = 2;
                        break;
                    case 4:
                        sb.Append("четыре ");
                        c = 2;
                        break;
                    case 5:
                        sb.Append("пять ");
                        c = 0;
                        break;
                    case 6:
                        sb.Append("шесть ");
                        c = 0;
                        break;
                    case 7:
                        sb.Append("семь ");
                        c = 0;
                        break;
                    case 8:
                        sb.Append("восемь ");
                        c = 0;
                        break;
                    case 9:
                        sb.Append("девять ");
                        c = 0;
                        break;
                }
            }
            s = sb.ToString();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://ardageo.ru");
        }

        private void numericUpDownInflation_ValueChanged(object sender, EventArgs e)
        {
            configManager["Inflation"] = numericUpDownInflation.Value.ToString(
                System.Globalization.NumberFormatInfo.InvariantInfo);
        }

        private void textBoxInflationReason_TextChanged(object sender, EventArgs e)
        {
            configManager["InflationReason"] = textBoxInflationReason.Text;
        }

        private void buttonPageSetup_Click(object sender, EventArgs e)
        {
            try
            {
                pageSetupDialog1.ShowDialog(this);
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ошибка при задании параметров печати может быть вызвана " +
                    "отсутствием установленных принтеров или неправильной работой драйвера принтера.\n" +
                    "Информация об ошибке:\n" + ex.Message, "Ошибка диалога параметров печати",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void linkLabelEula_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FormAbout formAbout = new FormAbout();
            formAbout.ShowDialog(this);
        }

        private void textBoxContractor_TextChanged(object sender, EventArgs e)
        {
            configManager["Contractor"] = textBoxContractor.Text;
        }

        private void comboBoxRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            Region region = regions[comboBoxRegion.Text];
            labelRegionWinter.Text = string.Format("Неблагоприятный период с {0} по {1}, коэффициент {2}",
                region.WinterStart, region.WinterEnd, region.WinterFactor);
            configManager["Region"] = comboBoxRegion.SelectedIndex.ToString(
                System.Globalization.NumberFormatInfo.InvariantInfo);
        }

        private void checkBoxWinter_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxRegion.Enabled = checkBoxWinter.Checked;
            labelRegionWinter.Enabled = checkBoxWinter.Checked;
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            configManager.WriteConfig();
        }

        private void checkBoxVAT_CheckedChanged(object sender, EventArgs e)
        {
            configManager["UseVAT"] = checkBoxVAT.Checked.ToString();
        }

        private void checkBoxSalaryFactor_CheckedChanged(object sender, EventArgs e)
        {
            comboBoxSalaryFactor.Enabled = checkBoxSalaryFactor.Checked;
            labelSalaryFactor.Enabled = checkBoxSalaryFactor.Checked;
        }

        private void comboBoxSalaryFactor_SelectedIndexChanged(object sender, EventArgs e)
        {
            double salaryFactor = (double)comboBoxSalaryFactor.SelectedItem;
            labelSalaryFactor.Text = string.Format("Коэффициент к итогу: {0}", SalaryFactorToPriceFactor(salaryFactor));
            configManager["SalaryFactor"] = comboBoxSalaryFactor.SelectedIndex.ToString(
                System.Globalization.NumberFormatInfo.InvariantInfo);
        }

        // Вычисляет коэффициент к итогу сметной стоимости на основе регионального
        // коэффициента к заработной плате
        private double SalaryFactorToPriceFactor(double salaryFactor)
        {
            switch ((int)(Math.Round(salaryFactor * 100)))
            {
                case 110: return 1.05;
                case 115: return 1.08;
                case 120: return 1.10;
                case 125: return 1.13;
                case 130: return 1.15;
                case 140: return 1.20;
                case 150: return 1.25;
                case 160: return 1.30;
                case 170: return 1.35;
                case 180: return 1.40;
                case 190: return 1.45;
                case 200: return 1.50;
                default: return 1.00;
            }
        }

        private void checkBoxRedLines_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxRedLinesAnalytic.Enabled = checkBoxRedLines.Checked;
        }
    }
}
