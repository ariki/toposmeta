﻿namespace TopoSmeta
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelIntDist = new System.Windows.Forms.Label();
            this.comboBoxIntDist = new System.Windows.Forms.ComboBox();
            this.comboBoxExtDist = new System.Windows.Forms.ComboBox();
            this.checkBoxVertical = new System.Windows.Forms.CheckBox();
            this.checkBoxUnderground = new System.Windows.Forms.CheckBox();
            this.checkBoxCameral = new System.Windows.Forms.CheckBox();
            this.checkBoxUpdate = new System.Windows.Forms.CheckBox();
            this.labelExtDist = new System.Windows.Forms.Label();
            this.checkBoxRegime = new System.Windows.Forms.CheckBox();
            this.comboBoxTerritory = new System.Windows.Forms.ComboBox();
            this.numericUpDownArea = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxWinter = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxNoFieldAllowances = new System.Windows.Forms.CheckBox();
            this.checkBoxComputer = new System.Windows.Forms.CheckBox();
            this.checkBoxColour = new System.Windows.Forms.CheckBox();
            this.checkBoxRestricted = new System.Windows.Forms.CheckBox();
            this.checkBoxIntermediate = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.numericUpDownHeights = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownCoordinates = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDownMaterials = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBoxRegistration = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxInflationReason = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDownInflation = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.linkLabelEula = new System.Windows.Forms.LinkLabel();
            this.linkLabelArda = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonEstimate = new System.Windows.Forms.Button();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.buttonPageSetup = new System.Windows.Forms.Button();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageGeneral = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.numericUpDownVAT = new System.Windows.Forms.NumericUpDown();
            this.checkBoxVAT = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.numericUpDownUrgency = new System.Windows.Forms.NumericUpDown();
            this.checkBoxUrgency = new System.Windows.Forms.CheckBox();
            this.checkBoxUnforeseen = new System.Windows.Forms.CheckBox();
            this.numericUpDownUnforeseen = new System.Windows.Forms.NumericUpDown();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBoxContractor = new System.Windows.Forms.TextBox();
            this.labelContractor = new System.Windows.Forms.Label();
            this.textBoxCustomer = new System.Windows.Forms.TextBox();
            this.labelCustomer = new System.Windows.Forms.Label();
            this.textBoxObjectName = new System.Windows.Forms.TextBox();
            this.labelObjectName = new System.Windows.Forms.Label();
            this.dateTimePickerContractDate = new System.Windows.Forms.DateTimePicker();
            this.labelContractDate = new System.Windows.Forms.Label();
            this.labelContractNumber = new System.Windows.Forms.Label();
            this.textBoxContractNumber = new System.Windows.Forms.TextBox();
            this.tabPageSurvey = new System.Windows.Forms.TabPage();
            this.groupBoxRegion = new System.Windows.Forms.GroupBox();
            this.labelSalaryFactor = new System.Windows.Forms.Label();
            this.comboBoxSalaryFactor = new System.Windows.Forms.ComboBox();
            this.checkBoxSalaryFactor = new System.Windows.Forms.CheckBox();
            this.labelRegionWinter = new System.Windows.Forms.Label();
            this.comboBoxRegion = new System.Windows.Forms.ComboBox();
            this.tabPageRegistration = new System.Windows.Forms.TabPage();
            this.checkBoxRedLines = new System.Windows.Forms.CheckBox();
            this.checkBoxRedLinesAnalytic = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownArea)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHeights)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoordinates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaterials)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInflation)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabControlMain.SuspendLayout();
            this.tabPageGeneral.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVAT)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUrgency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUnforeseen)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.tabPageSurvey.SuspendLayout();
            this.groupBoxRegion.SuspendLayout();
            this.tabPageRegistration.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 27);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Вид территории";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBoxRedLinesAnalytic);
            this.groupBox1.Controls.Add(this.checkBoxRedLines);
            this.groupBox1.Controls.Add(this.labelIntDist);
            this.groupBox1.Controls.Add(this.comboBoxIntDist);
            this.groupBox1.Controls.Add(this.comboBoxExtDist);
            this.groupBox1.Controls.Add(this.checkBoxVertical);
            this.groupBox1.Controls.Add(this.checkBoxUnderground);
            this.groupBox1.Controls.Add(this.checkBoxCameral);
            this.groupBox1.Controls.Add(this.checkBoxUpdate);
            this.groupBox1.Controls.Add(this.labelExtDist);
            this.groupBox1.Controls.Add(this.checkBoxRegime);
            this.groupBox1.Controls.Add(this.comboBoxTerritory);
            this.groupBox1.Controls.Add(this.numericUpDownArea);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(7, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(787, 237);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Выполнение работ по созданию инженерно-топографических планов";
            // 
            // labelIntDist
            // 
            this.labelIntDist.AutoSize = true;
            this.labelIntDist.Location = new System.Drawing.Point(8, 125);
            this.labelIntDist.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelIntDist.Name = "labelIntDist";
            this.labelIntDist.Size = new System.Drawing.Size(242, 17);
            this.labelIntDist.TabIndex = 6;
            this.labelIntDist.Text = "Расстояние от базы до участка, км";
            // 
            // comboBoxIntDist
            // 
            this.comboBoxIntDist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxIntDist.FormattingEnabled = true;
            this.comboBoxIntDist.Items.AddRange(new object[] {
            "до 5",
            "свыше 5 до 10",
            "свыше 10 до 15",
            "свыше 15 до 20",
            "свыше 20 до 30",
            "свыше 30 до 40",
            "свыше 40 до 50",
            "свыше 50 до 100"});
            this.comboBoxIntDist.Location = new System.Drawing.Point(283, 121);
            this.comboBoxIntDist.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxIntDist.Name = "comboBoxIntDist";
            this.comboBoxIntDist.Size = new System.Drawing.Size(160, 24);
            this.comboBoxIntDist.TabIndex = 7;
            // 
            // comboBoxExtDist
            // 
            this.comboBoxExtDist.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxExtDist.FormattingEnabled = true;
            this.comboBoxExtDist.Items.AddRange(new object[] {
            "до 25",
            "свыше 25 до 100",
            "свыше 100 до 300",
            "свыше 300 до 500",
            "свыше 500 до 1000",
            "свыше 1000 до 2000",
            "свыше 2000"});
            this.comboBoxExtDist.Location = new System.Drawing.Point(283, 89);
            this.comboBoxExtDist.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxExtDist.Name = "comboBoxExtDist";
            this.comboBoxExtDist.Size = new System.Drawing.Size(160, 24);
            this.comboBoxExtDist.TabIndex = 5;
            // 
            // checkBoxVertical
            // 
            this.checkBoxVertical.AutoSize = true;
            this.checkBoxVertical.Location = new System.Drawing.Point(456, 121);
            this.checkBoxVertical.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxVertical.Name = "checkBoxVertical";
            this.checkBoxVertical.Size = new System.Drawing.Size(225, 21);
            this.checkBoxVertical.TabIndex = 11;
            this.checkBoxVertical.Text = "Только вертикальная съемка";
            this.checkBoxVertical.UseVisualStyleBackColor = true;
            this.checkBoxVertical.CheckedChanged += new System.EventHandler(this.checkBoxVertical_CheckedChanged);
            // 
            // checkBoxUnderground
            // 
            this.checkBoxUnderground.AutoSize = true;
            this.checkBoxUnderground.Checked = true;
            this.checkBoxUnderground.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUnderground.Location = new System.Drawing.Point(11, 153);
            this.checkBoxUnderground.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxUnderground.Name = "checkBoxUnderground";
            this.checkBoxUnderground.Size = new System.Drawing.Size(335, 21);
            this.checkBoxUnderground.TabIndex = 8;
            this.checkBoxUnderground.Text = "Составление плана подземных коммуникаций";
            this.checkBoxUnderground.UseVisualStyleBackColor = true;
            this.checkBoxUnderground.CheckedChanged += new System.EventHandler(this.checkBoxUnderground_CheckedChanged);
            // 
            // checkBoxCameral
            // 
            this.checkBoxCameral.AutoSize = true;
            this.checkBoxCameral.Location = new System.Drawing.Point(456, 150);
            this.checkBoxCameral.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxCameral.Name = "checkBoxCameral";
            this.checkBoxCameral.Size = new System.Drawing.Size(111, 21);
            this.checkBoxCameral.TabIndex = 12;
            this.checkBoxCameral.Text = "Камерально";
            this.checkBoxCameral.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdate
            // 
            this.checkBoxUpdate.AutoSize = true;
            this.checkBoxUpdate.Location = new System.Drawing.Point(456, 92);
            this.checkBoxUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxUpdate.Name = "checkBoxUpdate";
            this.checkBoxUpdate.Size = new System.Drawing.Size(156, 21);
            this.checkBoxUpdate.TabIndex = 10;
            this.checkBoxUpdate.Text = "Обновление плана";
            this.checkBoxUpdate.UseVisualStyleBackColor = true;
            // 
            // labelExtDist
            // 
            this.labelExtDist.AutoSize = true;
            this.labelExtDist.Location = new System.Drawing.Point(8, 91);
            this.labelExtDist.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelExtDist.Name = "labelExtDist";
            this.labelExtDist.Size = new System.Drawing.Size(257, 17);
            this.labelExtDist.TabIndex = 4;
            this.labelExtDist.Text = "Расстояние в одном направлении, км";
            // 
            // checkBoxRegime
            // 
            this.checkBoxRegime.AutoSize = true;
            this.checkBoxRegime.Location = new System.Drawing.Point(456, 63);
            this.checkBoxRegime.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxRegime.Name = "checkBoxRegime";
            this.checkBoxRegime.Size = new System.Drawing.Size(105, 21);
            this.checkBoxRegime.TabIndex = 9;
            this.checkBoxRegime.Text = "Спецрежим";
            this.checkBoxRegime.UseVisualStyleBackColor = true;
            // 
            // comboBoxTerritory
            // 
            this.comboBoxTerritory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTerritory.FormattingEnabled = true;
            this.comboBoxTerritory.Location = new System.Drawing.Point(132, 23);
            this.comboBoxTerritory.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxTerritory.Name = "comboBoxTerritory";
            this.comboBoxTerritory.Size = new System.Drawing.Size(645, 24);
            this.comboBoxTerritory.TabIndex = 1;
            // 
            // numericUpDownArea
            // 
            this.numericUpDownArea.DecimalPlaces = 4;
            this.numericUpDownArea.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
            this.numericUpDownArea.Location = new System.Drawing.Point(167, 57);
            this.numericUpDownArea.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownArea.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDownArea.Name = "numericUpDownArea";
            this.numericUpDownArea.Size = new System.Drawing.Size(160, 22);
            this.numericUpDownArea.TabIndex = 3;
            this.numericUpDownArea.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 59);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Площадь съемки, га";
            // 
            // checkBoxWinter
            // 
            this.checkBoxWinter.AutoSize = true;
            this.checkBoxWinter.Location = new System.Drawing.Point(7, 22);
            this.checkBoxWinter.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxWinter.Name = "checkBoxWinter";
            this.checkBoxWinter.Size = new System.Drawing.Size(374, 21);
            this.checkBoxWinter.TabIndex = 0;
            this.checkBoxWinter.Text = "Выполнение работ в неблагоприятный период года";
            this.checkBoxWinter.UseVisualStyleBackColor = true;
            this.checkBoxWinter.CheckedChanged += new System.EventHandler(this.checkBoxWinter_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBoxNoFieldAllowances);
            this.groupBox2.Controls.Add(this.checkBoxComputer);
            this.groupBox2.Controls.Add(this.checkBoxColour);
            this.groupBox2.Controls.Add(this.checkBoxRestricted);
            this.groupBox2.Controls.Add(this.checkBoxIntermediate);
            this.groupBox2.Location = new System.Drawing.Point(7, 249);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(787, 82);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Коэффициенты к стоимости изыскательских работ";
            // 
            // checkBoxNoFieldAllowances
            // 
            this.checkBoxNoFieldAllowances.AutoSize = true;
            this.checkBoxNoFieldAllowances.Location = new System.Drawing.Point(456, 52);
            this.checkBoxNoFieldAllowances.Name = "checkBoxNoFieldAllowances";
            this.checkBoxNoFieldAllowances.Size = new System.Drawing.Size(267, 21);
            this.checkBoxNoFieldAllowances.TabIndex = 4;
            this.checkBoxNoFieldAllowances.Text = "Без выплаты полевого довольствия";
            this.checkBoxNoFieldAllowances.UseVisualStyleBackColor = true;
            // 
            // checkBoxComputer
            // 
            this.checkBoxComputer.AutoSize = true;
            this.checkBoxComputer.Checked = true;
            this.checkBoxComputer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxComputer.Location = new System.Drawing.Point(456, 23);
            this.checkBoxComputer.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxComputer.Name = "checkBoxComputer";
            this.checkBoxComputer.Size = new System.Drawing.Size(310, 21);
            this.checkBoxComputer.TabIndex = 3;
            this.checkBoxComputer.Text = "Использование компьютерных технологий";
            this.checkBoxComputer.UseVisualStyleBackColor = true;
            // 
            // checkBoxColour
            // 
            this.checkBoxColour.AutoSize = true;
            this.checkBoxColour.Location = new System.Drawing.Point(320, 23);
            this.checkBoxColour.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxColour.Name = "checkBoxColour";
            this.checkBoxColour.Size = new System.Drawing.Size(123, 21);
            this.checkBoxColour.TabIndex = 1;
            this.checkBoxColour.Text = "Цветной план";
            this.checkBoxColour.UseVisualStyleBackColor = true;
            // 
            // checkBoxRestricted
            // 
            this.checkBoxRestricted.AutoSize = true;
            this.checkBoxRestricted.Location = new System.Drawing.Point(12, 52);
            this.checkBoxRestricted.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxRestricted.Name = "checkBoxRestricted";
            this.checkBoxRestricted.Size = new System.Drawing.Size(406, 21);
            this.checkBoxRestricted.TabIndex = 2;
            this.checkBoxRestricted.Text = "Использование материалов ограниченного пользования";
            this.checkBoxRestricted.UseVisualStyleBackColor = true;
            // 
            // checkBoxIntermediate
            // 
            this.checkBoxIntermediate.AutoSize = true;
            this.checkBoxIntermediate.Location = new System.Drawing.Point(12, 23);
            this.checkBoxIntermediate.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxIntermediate.Name = "checkBoxIntermediate";
            this.checkBoxIntermediate.Size = new System.Drawing.Size(272, 21);
            this.checkBoxIntermediate.TabIndex = 0;
            this.checkBoxIntermediate.Text = "Выдача промежуточных материалов";
            this.checkBoxIntermediate.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 26);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Значение";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.numericUpDownHeights);
            this.groupBox3.Controls.Add(this.numericUpDownCoordinates);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.numericUpDownMaterials);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.checkBoxRegistration);
            this.groupBox3.Location = new System.Drawing.Point(7, 7);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(787, 151);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Регистрация изыскательских работ и услуги архивных фондов";
            // 
            // numericUpDownHeights
            // 
            this.numericUpDownHeights.Location = new System.Drawing.Point(355, 111);
            this.numericUpDownHeights.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownHeights.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownHeights.Name = "numericUpDownHeights";
            this.numericUpDownHeights.Size = new System.Drawing.Size(93, 22);
            this.numericUpDownHeights.TabIndex = 6;
            // 
            // numericUpDownCoordinates
            // 
            this.numericUpDownCoordinates.Location = new System.Drawing.Point(355, 80);
            this.numericUpDownCoordinates.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownCoordinates.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownCoordinates.Name = "numericUpDownCoordinates";
            this.numericUpDownCoordinates.Size = new System.Drawing.Size(93, 22);
            this.numericUpDownCoordinates.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 113);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(304, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "Выдача высот пунктов (количество пунктов)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 82);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(335, 17);
            this.label6.TabIndex = 3;
            this.label6.Text = "Выдача координат пунктов (количество пунктов)";
            // 
            // numericUpDownMaterials
            // 
            this.numericUpDownMaterials.Location = new System.Drawing.Point(696, 46);
            this.numericUpDownMaterials.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownMaterials.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDownMaterials.Name = "numericUpDownMaterials";
            this.numericUpDownMaterials.Size = new System.Drawing.Size(83, 22);
            this.numericUpDownMaterials.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 48);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(664, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Выдача во временное пользование материалов топографических съемок (количество пла" +
                "ншетов)";
            // 
            // checkBoxRegistration
            // 
            this.checkBoxRegistration.AutoSize = true;
            this.checkBoxRegistration.Location = new System.Drawing.Point(12, 23);
            this.checkBoxRegistration.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxRegistration.Name = "checkBoxRegistration";
            this.checkBoxRegistration.Size = new System.Drawing.Size(274, 21);
            this.checkBoxRegistration.TabIndex = 0;
            this.checkBoxRegistration.Text = "Регистрация инженерных изысканий";
            this.checkBoxRegistration.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxInflationReason);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.numericUpDownInflation);
            this.groupBox4.Location = new System.Drawing.Point(4, 214);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(787, 60);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Коэффициент инфляции";
            // 
            // textBoxInflationReason
            // 
            this.textBoxInflationReason.Location = new System.Drawing.Point(292, 23);
            this.textBoxInflationReason.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxInflationReason.Name = "textBoxInflationReason";
            this.textBoxInflationReason.Size = new System.Drawing.Size(485, 22);
            this.textBoxInflationReason.TabIndex = 3;
            this.textBoxInflationReason.Text = "Письмо Минстроя России № 8367-ЕС/08 от 15.05.2014 г.";
            this.textBoxInflationReason.TextChanged += new System.EventHandler(this.textBoxInflationReason_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(200, 26);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 17);
            this.label8.TabIndex = 2;
            this.label8.Text = "Основание";
            // 
            // numericUpDownInflation
            // 
            this.numericUpDownInflation.DecimalPlaces = 2;
            this.numericUpDownInflation.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownInflation.Location = new System.Drawing.Point(89, 23);
            this.numericUpDownInflation.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownInflation.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDownInflation.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericUpDownInflation.Name = "numericUpDownInflation";
            this.numericUpDownInflation.Size = new System.Drawing.Size(103, 22);
            this.numericUpDownInflation.TabIndex = 1;
            this.numericUpDownInflation.Value = new decimal(new int[] {
            370,
            0,
            0,
            131072});
            this.numericUpDownInflation.ValueChanged += new System.EventHandler(this.numericUpDownInflation_ValueChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.linkLabelEula);
            this.groupBox5.Controls.Add(this.linkLabelArda);
            this.groupBox5.Controls.Add(this.pictureBox1);
            this.groupBox5.Location = new System.Drawing.Point(12, 490);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(659, 64);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            // 
            // linkLabelEula
            // 
            this.linkLabelEula.AutoSize = true;
            this.linkLabelEula.LinkArea = new System.Windows.Forms.LinkArea(36, 24);
            this.linkLabelEula.Location = new System.Drawing.Point(133, 37);
            this.linkLabelEula.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkLabelEula.Name = "linkLabelEula";
            this.linkLabelEula.Size = new System.Drawing.Size(438, 20);
            this.linkLabelEula.TabIndex = 1;
            this.linkLabelEula.TabStop = true;
            this.linkLabelEula.Text = "Перед использованием ознакомьтесь с лицензионным соглашением";
            this.linkLabelEula.UseCompatibleTextRendering = true;
            this.linkLabelEula.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelEula_LinkClicked);
            // 
            // linkLabelArda
            // 
            this.linkLabelArda.AutoSize = true;
            this.linkLabelArda.LinkArea = new System.Windows.Forms.LinkArea(2, 10);
            this.linkLabelArda.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.linkLabelArda.Location = new System.Drawing.Point(132, 15);
            this.linkLabelArda.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkLabelArda.Name = "linkLabelArda";
            this.linkLabelArda.Size = new System.Drawing.Size(174, 20);
            this.linkLabelArda.TabIndex = 0;
            this.linkLabelArda.TabStop = true;
            this.linkLabelArda.Text = "© ООО «Арда», 2009, 2013";
            this.linkLabelArda.UseCompatibleTextRendering = true;
            this.linkLabelArda.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::TopoSmeta.Properties.Resources.logo88x31;
            this.pictureBox1.Location = new System.Drawing.Point(12, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(117, 38);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // buttonEstimate
            // 
            this.buttonEstimate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonEstimate.Location = new System.Drawing.Point(679, 526);
            this.buttonEstimate.Margin = new System.Windows.Forms.Padding(4);
            this.buttonEstimate.Name = "buttonEstimate";
            this.buttonEstimate.Size = new System.Drawing.Size(145, 28);
            this.buttonEstimate.TabIndex = 2;
            this.buttonEstimate.Text = "Смета";
            this.buttonEstimate.UseVisualStyleBackColor = true;
            this.buttonEstimate.Click += new System.EventHandler(this.buttonEstimate_Click);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.ShowIcon = false;
            this.printPreviewDialog1.Visible = false;
            // 
            // printDocument1
            // 
            this.printDocument1.DocumentName = "Смета";
            this.printDocument1.BeginPrint += new System.Drawing.Printing.PrintEventHandler(this.printDocument1_BeginPrint);
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // pageSetupDialog1
            // 
            this.pageSetupDialog1.Document = this.printDocument1;
            // 
            // buttonPageSetup
            // 
            this.buttonPageSetup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPageSetup.Location = new System.Drawing.Point(679, 497);
            this.buttonPageSetup.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPageSetup.Name = "buttonPageSetup";
            this.buttonPageSetup.Size = new System.Drawing.Size(145, 28);
            this.buttonPageSetup.TabIndex = 1;
            this.buttonPageSetup.Text = "Настройка печати";
            this.buttonPageSetup.UseVisualStyleBackColor = true;
            this.buttonPageSetup.Click += new System.EventHandler(this.buttonPageSetup_Click);
            // 
            // tabControlMain
            // 
            this.tabControlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMain.Controls.Add(this.tabPageGeneral);
            this.tabControlMain.Controls.Add(this.tabPageSurvey);
            this.tabControlMain.Controls.Add(this.tabPageRegistration);
            this.tabControlMain.Location = new System.Drawing.Point(12, 12);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(812, 478);
            this.tabControlMain.TabIndex = 0;
            // 
            // tabPageGeneral
            // 
            this.tabPageGeneral.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageGeneral.Controls.Add(this.groupBox8);
            this.tabPageGeneral.Controls.Add(this.groupBox7);
            this.tabPageGeneral.Controls.Add(this.groupBox6);
            this.tabPageGeneral.Controls.Add(this.groupBox4);
            this.tabPageGeneral.Location = new System.Drawing.Point(4, 25);
            this.tabPageGeneral.Name = "tabPageGeneral";
            this.tabPageGeneral.Size = new System.Drawing.Size(804, 449);
            this.tabPageGeneral.TabIndex = 2;
            this.tabPageGeneral.Text = "Общая информация";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.numericUpDownVAT);
            this.groupBox8.Controls.Add(this.checkBoxVAT);
            this.groupBox8.Location = new System.Drawing.Point(4, 282);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(787, 57);
            this.groupBox8.TabIndex = 3;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Налоги";
            // 
            // numericUpDownVAT
            // 
            this.numericUpDownVAT.DecimalPlaces = 1;
            this.numericUpDownVAT.Location = new System.Drawing.Point(89, 21);
            this.numericUpDownVAT.Name = "numericUpDownVAT";
            this.numericUpDownVAT.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownVAT.TabIndex = 1;
            this.numericUpDownVAT.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // checkBoxVAT
            // 
            this.checkBoxVAT.AutoSize = true;
            this.checkBoxVAT.Location = new System.Drawing.Point(7, 22);
            this.checkBoxVAT.Name = "checkBoxVAT";
            this.checkBoxVAT.Size = new System.Drawing.Size(80, 21);
            this.checkBoxVAT.TabIndex = 0;
            this.checkBoxVAT.Text = "НДС, %";
            this.checkBoxVAT.UseVisualStyleBackColor = true;
            this.checkBoxVAT.CheckedChanged += new System.EventHandler(this.checkBoxVAT_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.numericUpDownUrgency);
            this.groupBox7.Controls.Add(this.checkBoxUrgency);
            this.groupBox7.Controls.Add(this.checkBoxUnforeseen);
            this.groupBox7.Controls.Add(this.numericUpDownUnforeseen);
            this.groupBox7.Location = new System.Drawing.Point(4, 149);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(787, 58);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Дополнительные коэффициенты";
            // 
            // numericUpDownUrgency
            // 
            this.numericUpDownUrgency.DecimalPlaces = 2;
            this.numericUpDownUrgency.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownUrgency.Location = new System.Drawing.Point(657, 21);
            this.numericUpDownUrgency.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownUrgency.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownUrgency.Name = "numericUpDownUrgency";
            this.numericUpDownUrgency.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownUrgency.TabIndex = 3;
            this.numericUpDownUrgency.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // checkBoxUrgency
            // 
            this.checkBoxUrgency.AutoSize = true;
            this.checkBoxUrgency.Location = new System.Drawing.Point(437, 22);
            this.checkBoxUrgency.Name = "checkBoxUrgency";
            this.checkBoxUrgency.Size = new System.Drawing.Size(214, 21);
            this.checkBoxUrgency.TabIndex = 2;
            this.checkBoxUrgency.Text = "Коэффициент за срочность";
            this.checkBoxUrgency.UseVisualStyleBackColor = true;
            // 
            // checkBoxUnforeseen
            // 
            this.checkBoxUnforeseen.AutoSize = true;
            this.checkBoxUnforeseen.Checked = true;
            this.checkBoxUnforeseen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUnforeseen.Location = new System.Drawing.Point(7, 22);
            this.checkBoxUnforeseen.Name = "checkBoxUnforeseen";
            this.checkBoxUnforeseen.Size = new System.Drawing.Size(224, 21);
            this.checkBoxUnforeseen.TabIndex = 0;
            this.checkBoxUnforeseen.Text = "Непредвиденные расходы, %";
            this.checkBoxUnforeseen.UseVisualStyleBackColor = true;
            // 
            // numericUpDownUnforeseen
            // 
            this.numericUpDownUnforeseen.Location = new System.Drawing.Point(237, 21);
            this.numericUpDownUnforeseen.Name = "numericUpDownUnforeseen";
            this.numericUpDownUnforeseen.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownUnforeseen.TabIndex = 1;
            this.numericUpDownUnforeseen.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBoxContractor);
            this.groupBox6.Controls.Add(this.labelContractor);
            this.groupBox6.Controls.Add(this.textBoxCustomer);
            this.groupBox6.Controls.Add(this.labelCustomer);
            this.groupBox6.Controls.Add(this.textBoxObjectName);
            this.groupBox6.Controls.Add(this.labelObjectName);
            this.groupBox6.Controls.Add(this.dateTimePickerContractDate);
            this.groupBox6.Controls.Add(this.labelContractDate);
            this.groupBox6.Controls.Add(this.labelContractNumber);
            this.groupBox6.Controls.Add(this.textBoxContractNumber);
            this.groupBox6.Location = new System.Drawing.Point(4, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(787, 138);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Договор";
            // 
            // textBoxContractor
            // 
            this.textBoxContractor.Location = new System.Drawing.Point(129, 105);
            this.textBoxContractor.Name = "textBoxContractor";
            this.textBoxContractor.Size = new System.Drawing.Size(648, 22);
            this.textBoxContractor.TabIndex = 9;
            this.textBoxContractor.TextChanged += new System.EventHandler(this.textBoxContractor_TextChanged);
            // 
            // labelContractor
            // 
            this.labelContractor.AutoSize = true;
            this.labelContractor.Location = new System.Drawing.Point(6, 108);
            this.labelContractor.Name = "labelContractor";
            this.labelContractor.Size = new System.Drawing.Size(81, 17);
            this.labelContractor.TabIndex = 8;
            this.labelContractor.Text = "Подрядчик";
            // 
            // textBoxCustomer
            // 
            this.textBoxCustomer.Location = new System.Drawing.Point(129, 77);
            this.textBoxCustomer.Name = "textBoxCustomer";
            this.textBoxCustomer.Size = new System.Drawing.Size(648, 22);
            this.textBoxCustomer.TabIndex = 7;
            // 
            // labelCustomer
            // 
            this.labelCustomer.AutoSize = true;
            this.labelCustomer.Location = new System.Drawing.Point(6, 80);
            this.labelCustomer.Name = "labelCustomer";
            this.labelCustomer.Size = new System.Drawing.Size(70, 17);
            this.labelCustomer.TabIndex = 6;
            this.labelCustomer.Text = "Заказчик";
            // 
            // textBoxObjectName
            // 
            this.textBoxObjectName.Location = new System.Drawing.Point(129, 49);
            this.textBoxObjectName.Name = "textBoxObjectName";
            this.textBoxObjectName.Size = new System.Drawing.Size(648, 22);
            this.textBoxObjectName.TabIndex = 5;
            // 
            // labelObjectName
            // 
            this.labelObjectName.AutoSize = true;
            this.labelObjectName.Location = new System.Drawing.Point(6, 52);
            this.labelObjectName.Name = "labelObjectName";
            this.labelObjectName.Size = new System.Drawing.Size(58, 17);
            this.labelObjectName.TabIndex = 4;
            this.labelObjectName.Text = "Объект";
            // 
            // dateTimePickerContractDate
            // 
            this.dateTimePickerContractDate.Location = new System.Drawing.Point(369, 21);
            this.dateTimePickerContractDate.Name = "dateTimePickerContractDate";
            this.dateTimePickerContractDate.ShowCheckBox = true;
            this.dateTimePickerContractDate.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerContractDate.TabIndex = 3;
            // 
            // labelContractDate
            // 
            this.labelContractDate.AutoSize = true;
            this.labelContractDate.Location = new System.Drawing.Point(256, 24);
            this.labelContractDate.Name = "labelContractDate";
            this.labelContractDate.Size = new System.Drawing.Size(106, 17);
            this.labelContractDate.TabIndex = 2;
            this.labelContractDate.Text = "Дата договора";
            // 
            // labelContractNumber
            // 
            this.labelContractNumber.AutoSize = true;
            this.labelContractNumber.Location = new System.Drawing.Point(6, 24);
            this.labelContractNumber.Name = "labelContractNumber";
            this.labelContractNumber.Size = new System.Drawing.Size(115, 17);
            this.labelContractNumber.TabIndex = 0;
            this.labelContractNumber.Text = "Номер договора";
            // 
            // textBoxContractNumber
            // 
            this.textBoxContractNumber.Location = new System.Drawing.Point(129, 21);
            this.textBoxContractNumber.Name = "textBoxContractNumber";
            this.textBoxContractNumber.Size = new System.Drawing.Size(120, 22);
            this.textBoxContractNumber.TabIndex = 1;
            // 
            // tabPageSurvey
            // 
            this.tabPageSurvey.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageSurvey.Controls.Add(this.groupBoxRegion);
            this.tabPageSurvey.Controls.Add(this.groupBox1);
            this.tabPageSurvey.Controls.Add(this.groupBox2);
            this.tabPageSurvey.Location = new System.Drawing.Point(4, 25);
            this.tabPageSurvey.Name = "tabPageSurvey";
            this.tabPageSurvey.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSurvey.Size = new System.Drawing.Size(804, 449);
            this.tabPageSurvey.TabIndex = 0;
            this.tabPageSurvey.Text = "Изыскания";
            // 
            // groupBoxRegion
            // 
            this.groupBoxRegion.Controls.Add(this.labelSalaryFactor);
            this.groupBoxRegion.Controls.Add(this.comboBoxSalaryFactor);
            this.groupBoxRegion.Controls.Add(this.checkBoxSalaryFactor);
            this.groupBoxRegion.Controls.Add(this.labelRegionWinter);
            this.groupBoxRegion.Controls.Add(this.comboBoxRegion);
            this.groupBoxRegion.Controls.Add(this.checkBoxWinter);
            this.groupBoxRegion.Location = new System.Drawing.Point(7, 338);
            this.groupBoxRegion.Name = "groupBoxRegion";
            this.groupBoxRegion.Size = new System.Drawing.Size(787, 105);
            this.groupBoxRegion.TabIndex = 2;
            this.groupBoxRegion.TabStop = false;
            this.groupBoxRegion.Text = "Региональные коэффициенты";
            // 
            // labelSalaryFactor
            // 
            this.labelSalaryFactor.AutoSize = true;
            this.labelSalaryFactor.Enabled = false;
            this.labelSalaryFactor.Location = new System.Drawing.Point(456, 77);
            this.labelSalaryFactor.Name = "labelSalaryFactor";
            this.labelSalaryFactor.Size = new System.Drawing.Size(212, 17);
            this.labelSalaryFactor.TabIndex = 5;
            this.labelSalaryFactor.Text = "Коэффициент не применяется";
            // 
            // comboBoxSalaryFactor
            // 
            this.comboBoxSalaryFactor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSalaryFactor.Enabled = false;
            this.comboBoxSalaryFactor.FormattingEnabled = true;
            this.comboBoxSalaryFactor.Location = new System.Drawing.Point(456, 50);
            this.comboBoxSalaryFactor.Name = "comboBoxSalaryFactor";
            this.comboBoxSalaryFactor.Size = new System.Drawing.Size(325, 24);
            this.comboBoxSalaryFactor.TabIndex = 4;
            this.comboBoxSalaryFactor.SelectedIndexChanged += new System.EventHandler(this.comboBoxSalaryFactor_SelectedIndexChanged);
            // 
            // checkBoxSalaryFactor
            // 
            this.checkBoxSalaryFactor.AutoSize = true;
            this.checkBoxSalaryFactor.Location = new System.Drawing.Point(456, 21);
            this.checkBoxSalaryFactor.Name = "checkBoxSalaryFactor";
            this.checkBoxSalaryFactor.Size = new System.Drawing.Size(328, 21);
            this.checkBoxSalaryFactor.TabIndex = 3;
            this.checkBoxSalaryFactor.Text = "Районный коэффициент к заработной плате";
            this.checkBoxSalaryFactor.UseVisualStyleBackColor = true;
            this.checkBoxSalaryFactor.CheckedChanged += new System.EventHandler(this.checkBoxSalaryFactor_CheckedChanged);
            // 
            // labelRegionWinter
            // 
            this.labelRegionWinter.AutoSize = true;
            this.labelRegionWinter.Enabled = false;
            this.labelRegionWinter.Location = new System.Drawing.Point(6, 77);
            this.labelRegionWinter.Name = "labelRegionWinter";
            this.labelRegionWinter.Size = new System.Drawing.Size(127, 17);
            this.labelRegionWinter.TabIndex = 2;
            this.labelRegionWinter.Text = "Регион не выбран";
            // 
            // comboBoxRegion
            // 
            this.comboBoxRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRegion.Enabled = false;
            this.comboBoxRegion.FormattingEnabled = true;
            this.comboBoxRegion.Location = new System.Drawing.Point(6, 50);
            this.comboBoxRegion.Name = "comboBoxRegion";
            this.comboBoxRegion.Size = new System.Drawing.Size(437, 24);
            this.comboBoxRegion.TabIndex = 1;
            this.comboBoxRegion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegion_SelectedIndexChanged);
            // 
            // tabPageRegistration
            // 
            this.tabPageRegistration.BackColor = System.Drawing.SystemColors.Control;
            this.tabPageRegistration.Controls.Add(this.groupBox3);
            this.tabPageRegistration.Location = new System.Drawing.Point(4, 25);
            this.tabPageRegistration.Name = "tabPageRegistration";
            this.tabPageRegistration.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRegistration.Size = new System.Drawing.Size(804, 449);
            this.tabPageRegistration.TabIndex = 1;
            this.tabPageRegistration.Text = "Регистрация работ";
            // 
            // checkBoxRedLines
            // 
            this.checkBoxRedLines.AutoSize = true;
            this.checkBoxRedLines.Location = new System.Drawing.Point(11, 181);
            this.checkBoxRedLines.Name = "checkBoxRedLines";
            this.checkBoxRedLines.Size = new System.Drawing.Size(205, 21);
            this.checkBoxRedLines.TabIndex = 13;
            this.checkBoxRedLines.Text = "Нанесение красных линий";
            this.checkBoxRedLines.UseVisualStyleBackColor = true;
            this.checkBoxRedLines.CheckedChanged += new System.EventHandler(this.checkBoxRedLines_CheckedChanged);
            // 
            // checkBoxRedLinesAnalytic
            // 
            this.checkBoxRedLinesAnalytic.AutoSize = true;
            this.checkBoxRedLinesAnalytic.Enabled = false;
            this.checkBoxRedLinesAnalytic.Location = new System.Drawing.Point(222, 181);
            this.checkBoxRedLinesAnalytic.Name = "checkBoxRedLinesAnalytic";
            this.checkBoxRedLinesAnalytic.Size = new System.Drawing.Size(178, 21);
            this.checkBoxRedLinesAnalytic.TabIndex = 14;
            this.checkBoxRedLinesAnalytic.Text = "с расчётом координат";
            this.checkBoxRedLinesAnalytic.UseVisualStyleBackColor = true;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 567);
            this.Controls.Add(this.tabControlMain);
            this.Controls.Add(this.buttonPageSetup);
            this.Controls.Add(this.buttonEstimate);
            this.Controls.Add(this.groupBox5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.Text = "Топосмета — расчет стоимости топографической съемки";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownArea)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHeights)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCoordinates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMaterials)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownInflation)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabControlMain.ResumeLayout(false);
            this.tabPageGeneral.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVAT)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUrgency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUnforeseen)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tabPageSurvey.ResumeLayout(false);
            this.groupBoxRegion.ResumeLayout(false);
            this.groupBoxRegion.PerformLayout();
            this.tabPageRegistration.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDownArea;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxTerritory;
        private System.Windows.Forms.CheckBox checkBoxWinter;
        private System.Windows.Forms.CheckBox checkBoxRegime;
        private System.Windows.Forms.Label labelExtDist;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxColour;
        private System.Windows.Forms.CheckBox checkBoxRestricted;
        private System.Windows.Forms.CheckBox checkBoxIntermediate;
        private System.Windows.Forms.CheckBox checkBoxComputer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numericUpDownInflation;
        private System.Windows.Forms.CheckBox checkBoxUpdate;
        private System.Windows.Forms.CheckBox checkBoxUnderground;
        private System.Windows.Forms.CheckBox checkBoxCameral;
        private System.Windows.Forms.CheckBox checkBoxVertical;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numericUpDownMaterials;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBoxRegistration;
        private System.Windows.Forms.NumericUpDown numericUpDownHeights;
        private System.Windows.Forms.NumericUpDown numericUpDownCoordinates;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBoxInflationReason;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button buttonEstimate;
        private System.Windows.Forms.ComboBox comboBoxExtDist;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkLabelArda;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.Button buttonPageSetup;
        private System.Windows.Forms.LinkLabel linkLabelEula;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageGeneral;
        private System.Windows.Forms.TabPage tabPageSurvey;
        private System.Windows.Forms.TabPage tabPageRegistration;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DateTimePicker dateTimePickerContractDate;
        private System.Windows.Forms.Label labelContractDate;
        private System.Windows.Forms.Label labelContractNumber;
        private System.Windows.Forms.TextBox textBoxContractNumber;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.NumericUpDown numericUpDownVAT;
        private System.Windows.Forms.CheckBox checkBoxVAT;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.NumericUpDown numericUpDownUrgency;
        private System.Windows.Forms.CheckBox checkBoxUrgency;
        private System.Windows.Forms.CheckBox checkBoxUnforeseen;
        private System.Windows.Forms.NumericUpDown numericUpDownUnforeseen;
        private System.Windows.Forms.TextBox textBoxContractor;
        private System.Windows.Forms.Label labelContractor;
        private System.Windows.Forms.TextBox textBoxCustomer;
        private System.Windows.Forms.Label labelCustomer;
        private System.Windows.Forms.TextBox textBoxObjectName;
        private System.Windows.Forms.Label labelObjectName;
        private System.Windows.Forms.GroupBox groupBoxRegion;
        private System.Windows.Forms.ComboBox comboBoxRegion;
        private System.Windows.Forms.Label labelRegionWinter;
        private System.Windows.Forms.Label labelSalaryFactor;
        private System.Windows.Forms.ComboBox comboBoxSalaryFactor;
        private System.Windows.Forms.CheckBox checkBoxSalaryFactor;
        private System.Windows.Forms.CheckBox checkBoxNoFieldAllowances;
        private System.Windows.Forms.Label labelIntDist;
        private System.Windows.Forms.ComboBox comboBoxIntDist;
        private System.Windows.Forms.CheckBox checkBoxRedLinesAnalytic;
        private System.Windows.Forms.CheckBox checkBoxRedLines;
    }
}

